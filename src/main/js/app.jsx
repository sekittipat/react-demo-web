// import 'typeface-roboto';

import React, { Suspense, lazy } from 'react';
import { connect } from 'react-redux';
import { Redirect, Route, Switch } from 'react-router-dom';

import ErrorPage from './pages/error.jsx';
import LoadingPage from './pages/loading.jsx';
import LoginPage from './pages/login.jsx';
import LogoutPage from './pages/logout.jsx';
import MainPage from './pages/main.jsx';
import MainTemplate from './templates/main.jsx';
import { Roles } from './utils/constants.jsx';
import Logger from './utils/logger.jsx';
import { sessionManager } from './utils/session.jsx';

// import LineAlertJobPage from './pages/line-alert/line-alert-job.jsx';

// import UserPage from './pages/admin/user.jsx';

// import MasterDataPage from './pages/admin/master_data.jsx';

// import '../scss/bootstrap/custom.scss';
// import '../scss/react-widgets/custom.scss';
// import '../scss/app.scss';

// import Menu5Page from './pages/menu5.jsx';
// import EmployeePage from './pages/employee.jsx';

/**
 * Custom function check user is already authenticated before render request page,
 * otherwise, redirect to login page
 */
const _ = require('underscore');

const PrivateRoute = ({ component: Component, path, roles, ...rest }) => {
  return (
    <Route
      {...rest}
      render={props => {
        Logger.console.debug('Route Path: ', path);
        Logger.console.debug('Route Roles: ', roles);
        if (sessionManager.isUserLoggedIn()) {
          if (roles && roles.length > 0) {
            let userLogin = sessionManager.getLoginUser();
            if (_.intersection(roles, userLogin.roles).length > 0) {
              return <Component {...props} />;
            } else {
              Logger.console.debug(
                'Route Permission Denied: ',
                userLogin.username
              );
            }
          } else {
            return <Component {...props} />;
          }
        }
        // redirect to login page when not logged in or invalid permission roles
        return (
          <Redirect
            to={{
              pathname: '/page/logout',
              state: { from: props.location }
            }}
          />
        );
      }}
    />
  );
};

/**
 * Async loading component
 */
/* Admin Pages */
const UserPageAsync = lazy(() => import('./pages/admin/user.jsx'));
// const DataSourcePageAsync = lazy(() => import('./pages/admin/data-source.jsx'));
const DBDataSourcePageAsync = lazy(() =>
  import('./pages/admin/data-source/db-data-source.jsx')
);
const RestAPIDataSourcePageAsync = lazy(() =>
  import('./pages/admin/data-source/restapi-data-source.jsx')
);
const TextFieldTestPageAsync = lazy(() =>
  import('./pages/admin/component-test/textfield.jsx')
);
/* Line Alert Job Pages */
const LineAlertJobPageAsync = lazy(() =>
  import('./pages/line-alert/line-alert-job.jsx')
);
/* RTPMS Pages */
const RTPMSConditionPageAsync = lazy(() =>
  import('./pages/rtpms/condition.jsx')
);
const RTPMSPointGroupPageAsync = lazy(() =>
  import('./pages/rtpms/point-group.jsx')
);

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  componentDidMount() {}

  render() {
    let pageUrl =
      window.appContext.pageUrl !== undefined &&
      window.appContext.pageUrl.startsWith('/page')
        ? window.appContext.pageUrl
        : '/page/main';

    return (
      <MainTemplate>
        <Suspense fallback={this.renderLoadingPage()}>
          <Switch>
            <Route
              path="/page/login"
              exact={true}
              render={props => {
                if (sessionManager.isUserLoggedIn()) {
                  return (
                    <Redirect
                      to={{
                        pathname: '/page/main',
                        state: { from: props.location }
                      }}
                    />
                  );
                }

                return <LoginPage {...props} />;
              }}
            />
            <PrivateRoute path="/page/main" exact={true} component={MainPage} />
            <PrivateRoute
              path="/page/admin/user"
              exact={true}
              component={UserPageAsync}
              roles={[Roles.ROLE_ADMIN]}
            />
            {/* <PrivateRoute
              path="/page/admin/dataSource"
              exact={true}
              component={DataSourcePageAsync}
              roles={[Roles.ROLE_ADMIN]}
            /> */}
            <PrivateRoute
              path="/page/admin/dataSource/database"
              exact={true}
              component={DBDataSourcePageAsync}
              roles={[Roles.ROLE_ADMIN]}
            />
            <PrivateRoute
              path="/page/admin/dataSource/restAPI"
              exact={true}
              component={RestAPIDataSourcePageAsync}
              roles={[Roles.ROLE_ADMIN]}
            />
            <PrivateRoute
              path="/page/admin/componentTest/textfield"
              exact={true}
              component={TextFieldTestPageAsync}
              roles={[Roles.ROLE_ADMIN]}
            />
            <PrivateRoute
              path="/page/lineAlert/lineAlertJob"
              exact={true}
              component={LineAlertJobPageAsync}
            />
            <PrivateRoute
              path="/page/rtpms/condition"
              exact={true}
              component={RTPMSConditionPageAsync}
            />
            <PrivateRoute
              path="/page/rtpms/pointGroup"
              exact={true}
              component={RTPMSPointGroupPageAsync}
            />
            <Route path="/page/logout" component={LogoutPage} />
            <Route component={ErrorPage} /> {/* Handle not found route */}
            <Redirect to={pageUrl} />
          </Switch>
        </Suspense>
      </MainTemplate>
    );
  }

  renderLoadingPage = () => {
    return <LoadingPage />;
  };
}

const mapStateToProps = state => {
  return {
    login: state.login
  };
};

const mapDispatchToProps = dispatch => ({});

export default connect(mapStateToProps, mapDispatchToProps)(App);
