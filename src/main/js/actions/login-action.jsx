import { sessionManager } from '../utils/session.jsx';

export const LoginActionTypes = {
  LOGIN_SUCESS: 'LOGIN_SUCCESS',
  LOGOUT_SUCESS: 'LOGOUT_SUCCESS'
};

export const loginSuccess = user => {
  sessionManager.setLoginUser(user);
  return {
    type: LoginActionTypes.LOGIN_SUCESS,
    user: user
  };
};

export const logoutSuccess = () => {
  sessionManager.clearLoginUser();
  return {
    type: LoginActionTypes.LOGOUT_SUCESS
  };
};
