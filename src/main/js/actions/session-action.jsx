export const SessionActionTypes = {
  UPDATE_TIMER: 'UPDATE_TIMER'
};

export const updateTimer = remainingTimeSec => {
  return {
    type: SessionActionTypes.UPDATE_TIMER,
    remainingTimeSec: remainingTimeSec
  };
};
