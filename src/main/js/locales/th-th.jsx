import * as dataSource from './pages/admin/data-source.jsx';
import * as user from './pages/admin/user.jsx';
import * as lineAlertJob from './pages/line-alert/line-alert-job.jsx';
import * as rtpmsCondition from './pages/rtpms/condition.jsx';

const th_TH = {
  menu: {
    login: 'เข้าสู่ระบบ',
    logout: 'ออกจากระบบ',
    home: 'หน้าแรก',
    admin: 'ผู้ดูแลระบบ',
    // masterData: 'จัดการข้อมูลหลัก',
    // menuAndScreen: 'จัดการเมนูและหน้าจอ',
    user: 'ผู้ใช้งานระบบ',
    dataSource: 'แหล่งข้อมูล',
    dataSourceDB: 'Database',
    dataSourceRestAPI: 'Rest API',
    main: 'เมนูหลัก',
    lineAlert: 'Line Alert',
    lineAlertJob: 'Line Alert Job',
    lineAlertJobDetail: 'Line Alert Job Detail',
    // smsTag: 'ไฟล์ป้ายราคา',
    // printHistory: 'ประวัติการพิมพ์ป้ายราคา',
    // noImageTag: 'ป้ายราคาไม่มีรูปภาพ',
    // pdaFileUpload: 'ค้นหาข้อมูลจากไฟล์ PDA'
    rtpms: 'RTPMS',
    rtpmsCondition: 'Condition',
    rtpmsPointGroup: 'Point Group',
    componentTest: 'Component Test',
    componentTest_Textfield: 'TextField'
  },
  error: {
    requiredField: 'จำเป็นต้องกรอก',
    invalidCredential: 'ชื่อผู้ใช้งาน หรือ รหัสผ่าน ไม่ถูกต้อง'
  },
  timer: {
    timeoutWarning1: 'Session กำลังจะหมดอายุใน',
    timeoutWarning2: 'กรุณากดปุ่ม ใช้งานต่อ ถ้าต้องการอยู่ในเว็บไซต์นี้ต่อไป',
    timeoutWarning3: 'ถ้า session หมดอายุ กรุณาเข้าสู่ระบบใหม่อีกครั้ง',
    sec: 'วินาที',
    continue: 'ใช้งานต่อ'
  },
  topBar: {
    store: 'สาขา'
  },
  loginPage: {
    login: 'เข้าสู่ระบบ',
    username: 'ชื่อผู้ใช้งาน',
    password: 'รหัสผ่าน',
    hint: 'กรุณากรอก %{label}'
  },
  mainPage: {
    welcome: 'ยินดีต้อนรับ'
  },
  logoutPage: {
    logout: 'กำลังออกจากระบบ รอซักครู่...'
  },
  tooltip: {
    openMenu: 'แสดงเมนู',
    changeLanguage: 'เปลี่ยนภาษา'
  },
  loadingPage: {
    pageLoading: 'กำลังเปลี่ยนหน้า รอซักครู่...'
  },
  pages: {
    admin: {
      user: user.th_TH,
      dataSource: dataSource.th_TH
    },
    lineAlert: {
      lineAlertJob: lineAlertJob.th_TH
    },
    rtpms: {
      condition: rtpmsCondition.th_TH
    }
  },
  dataTable: {
    add: 'เพิ่ม',
    edit: 'แก้ไข',
    delete: 'ลบ',
    refresh: 'รีเฟรช',
    searchColumns: 'ระบุคอลัมน์',
    clear: 'เคลียร์',
    empty: 'ไม่พบข้อมูล',
    loading: 'กำลังดึงข้อมูล',
    view: 'ดู',
    selectAll: 'เลือกทั้งหมด',
    deselectAll: 'ไม่เลือกทั้งหมด',
    deleteAll: 'ลบทั้งหมด',
    moreActions: 'เพิ่มเติม',
    rowsSelected: 'แถวที่ถูกเลือก',
    select: 'เลือก'
  },
  dataFormDialog: {
    addTitle: 'เพิ่ม %{title}',
    updateTitle: 'แก้ไข %{title}',
    viewTitle: 'รายละเอียด %{title}'
  },
  actions: {
    save: 'บันทึก',
    cancel: 'ยกเลิก',
    confirm: 'ยืนยัน'
  }
};

export default th_TH;
