export const en_US = {
  lineAlertJobTable: {
    jobId: 'Job ID',
    jobName: 'Job Name',
    dsType: 'Data Source Type',
    dsName: 'Data Source Name',
    dsConfig: 'Data Source Config',
    alertType: 'Alert Type',
    alertNotFound: 'Alert When Not Found',
    errorNotFound: 'Error When Not Found',
    columnFirstRow: 'Column On First Row',
    alertGroup: 'Alert Group',
    cronExp: 'Cron Expression',
    activeFlag: 'Active Flag',
    createDate: 'Create Date',
    createBy: 'Create Date',
    lupdDate: 'Lupd Date',
    lupdBy: 'Lupd By',
    testRunJob: 'Test Run Job',
    lastRun: 'Last Run',
    nextRun: 'Next Run',
    dynamicTitle: 'Dynamic Title'
  }
};

export const th_TH = {
  lineAlertJobTable: {
    jobId: 'Job ID',
    jobName: 'Job Name',
    dsType: 'Data Source Type',
    dsName: 'Data Source Name',
    dsConfig: 'Data Source Config',
    alertType: 'Alert Type',
    alertNotFound: 'Alert When Not Found',
    errorNotFound: 'Error When Not Found',
    columnFirstRow: 'Column On First Row',
    alertGroup: 'Alert Group',
    cronExp: 'Cron Expression',
    activeFlag: 'Active Flag',
    createDate: 'Create Date',
    createBy: 'Create Date',
    lupdDate: 'Lupd Date',
    lupdBy: 'Lupd By',
    testRunJob: 'Test Run Job',
    lastRun: 'Last Run',
    nextRun: 'Next Run',
    dynamicTitle: 'Dynamic Title'
  }
};
