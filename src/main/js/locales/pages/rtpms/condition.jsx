export const en_US = {
  conditionTable: {
    conditionId: 'Condition ID',
    conditionThaiName: 'Condition Thai Name',
    conditionEngName: 'Condition Eng Name',
    startDate: 'Start Date',
    endDate: 'End Date',
    pointGroupId: 'Point Group',
    lupdDate: 'Lupd Date',
    lupdBy: 'Lupd By',
    pointType: 'Point Type'
  },
  actions: {
    activeOnly: 'Only Active',
    calculableOnly: 'Only Calculable'
  },
  storeConditionTable: {
    title: 'Store Condition',
    conditionId: 'ConditionID',
    storeId: 'Store ID',
    storeCode: 'Store Code',
    thaiName: 'Thai Name',
    engName: 'Eng Name',
    shortName: 'Short Name',
    storeFormat: 'Store Format',
    locNumber: 'Lock Number',
    lupdDate: 'Lupd Date',
    lupdBy: 'Lupd By'
  },
  pointGroupTable: {
    title: 'Point Group',
    pointGroupId: 'Point Group ID',
    pointGroupTHDesc: 'Thai Description',
    pointGroupENDesc: 'English Description',
    lupdDate: 'Last Update',
    lupdBy: 'Last Update By',
    activeFlag: 'Active Flag',
    roundingId: 'Rounding ID'
  }
};

export const th_TH = {
  conditionTable: {
    conditionId: 'Condition ID',
    conditionThaiName: 'Condition Thai Name',
    conditionEngName: 'Condition Eng Name',
    startDate: 'Start Date',
    endDate: 'End Date',
    pointGroupId: 'Point Group',
    lupdDate: 'Lupd Date',
    lupdBy: 'Lupd By',
    pointType: 'Point Type'
  },
  actions: {
    activeOnly: 'เฉพาะที่ Active',
    calculableOnly: 'เฉพาะที่คำนวณได้'
  },
  storeConditionTable: {
    title: 'Store Condition',
    conditionId: 'ConditionID',
    storeId: 'Store ID',
    storeCode: 'Store Code',
    thaiName: 'Thai Name',
    engName: 'Eng Name',
    shortName: 'Short Name',
    storeFormat: 'Store Format',
    locNumber: 'Lock Number',
    lupdDate: 'Lupd Date',
    lupdBy: 'Lupd By'
  },
  pointGroupTable: {
    title: 'Point Group',
    pointGroupId: 'Point Group ID',
    pointGroupTHDesc: 'Thai Description',
    pointGroupENDesc: 'English Description',
    lupdDate: 'Last Update',
    lupdBy: 'Last Update By',
    activeFlag: 'Active Flag',
    roundingId: 'Rounding ID'
  }
};
