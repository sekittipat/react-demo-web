export const en_US = {
  dataSourceTable: {
    dsId: 'DS ID',
    dsGroup: 'DS Group',
    activeFlag: 'Active',
    createDate: 'Create Date',
    createBy: 'Create By',
    lupdDate: 'Last Update Date',
    lupdBy: 'Last Update By'
  },
  dbDataSourceTable: {
    jdbcUrl: 'Jdbc Url',
    driverClassName: 'Driver Class Name',
    username: 'Username',
    password: 'Password'
  },
  restAPIDataSourceTable: {
    apiUrl: 'API Url',
    clientId: 'Client ID',
    clientSecret: 'Client Secret'
  }
};

export const th_TH = {
  dataSourceTable: {
    dsId: 'DS ID',
    dsGroup: 'DS Group',
    activeFlag: 'Active',
    createDate: 'Create Date',
    createBy: 'Create By',
    lupdDate: 'Last Update Date',
    lupdBy: 'Last Update By'
  },
  dbDataSourceTable: {
    jdbcUrl: 'Jdbc Url',
    driverClassName: 'Driver Class Name',
    username: 'Username',
    password: 'Password'
  },
  restAPIDataSourceTable: {
    apiUrl: 'API Url',
    clientId: 'Client ID',
    clientSecret: 'Client Secret'
  }
};
