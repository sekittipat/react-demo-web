export const en_US = {
  userTable: {
    userId: 'User ID',
    userPwd: 'Password',
    accountType: 'Account Type',
    email: 'E-mail',
    userRole: 'User Role',
    activeFlag: 'Active',
    requireChangePwd: 'Required Change Password',
    lastLoginDate: 'Last Login',
    lastChangePwd: 'Last Change Password',
    createDate: 'Create Date',
    createBy: 'Create By',
    lupdDate: 'Last Update Date',
    lupdBy: 'Last Update By',
    domainName: 'Domain Name'
  }
};

export const th_TH = {
  userTable: {
    userId: 'รหัสผู้ใช้งาน',
    userPwd: 'รหัสผ่าน',
    accountType: 'ประเภทผู้ใช้งาน',
    email: 'อีเมล',
    userRole: 'กลุ่มผู้ใช้งาน',
    activeFlag: 'ใช้งานได้',
    requireChangePwd: 'บังคับเปลี่ยนรหัสผ่าน',
    lastLoginDate: 'เข้าระบบล่าสุด',
    lastChangePwd: 'เปลี่ยนรหัสผ่านล่าสุด',
    createDate: 'สร้างเมื่อ',
    createBy: 'สร้างโดย',
    lupdDate: 'แก้ไขล่าสุดเมื่อ',
    lupdBy: 'แก้ไขล่าสุดโดย',
    domainName: 'ชื่อโดเมน'
  }
};
