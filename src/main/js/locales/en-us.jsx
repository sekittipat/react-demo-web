import * as dataSource from './pages/admin/data-source.jsx';
import * as user from './pages/admin/user.jsx';
import * as lineAlertJob from './pages/line-alert/line-alert-job.jsx';
import * as rtpmsCondition from './pages/rtpms/condition.jsx';

const en_US = {
  menu: {
    login: 'Login',
    logout: 'Logout',
    home: 'Home',
    admin: 'Administrator',
    // masterData: 'Master Data',
    // menuAndScreen: 'Menu and Screen',
    user: 'Users',
    dataSource: 'Data Sources',
    dataSourceDB: 'Database',
    dataSourceRestAPI: 'Rest API',
    main: 'Main Menu',
    lineAlert: 'Line Alert',
    lineAlertJob: 'Line Alert Job',
    lineAlertJobDetail: 'Line Alert Job Detail',
    // smsTag: 'Smart Shelf Tag',
    // printHistory: 'Print History',
    // noImageTag: 'No Image Tag',
    // pdaFileUpload: 'PDA File Upload'
    rtpms: 'RTPMS',
    rtpmsCondition: 'Condition',
    rtpmsPointGroup: 'Point Group',
    componentTest: 'Component Test',
    componentTest_Textfield: 'TextField'
  },
  error: {
    requiredField: 'This field is required',
    invalidCredential: 'Username or password is invalid'
  },
  timer: {
    timeoutWarning1: 'Session will be timeout in',
    timeoutWarning2: 'Please click continue if you want to stay in this site.',
    timeoutWarning3: 'After timeout, you need to re-login again.',
    sec: 'sec',
    continue: 'Continue'
  },
  topBar: {
    store: 'Store'
  },
  loginPage: {
    login: 'Login',
    username: 'Username',
    password: 'Password',
    hint: '%{label} is required'
  },
  mainPage: {
    welcome: 'Welcome'
  },
  logoutPage: {
    logout: 'Logging out. Please wait...'
  },
  tooltip: {
    openMenu: 'Open Menu',
    changeLanguage: 'Change Language'
  },
  loadingPage: {
    pageLoading: 'Page loading. Please wait...'
  },
  pages: {
    admin: {
      user: user.en_US,
      dataSource: dataSource.en_US
    },
    lineAlert: {
      lineAlertJob: lineAlertJob.en_US
    },
    rtpms: {
      condition: rtpmsCondition.en_US
    }
  },
  dataTable: {
    add: 'Add',
    edit: 'Edit',
    delete: 'Delete',
    refresh: 'Refresh',
    searchColumns: 'Search Columns',
    clear: 'Clear',
    empty: 'No records',
    loading: 'Data loading ...',
    view: 'View',
    selectAll: 'Select All',
    deselectAll: 'Deselect All',
    deleteAll: 'Delete All',
    moreActions: 'More actions',
    rowsSelected: 'row(s) selected',
    select: 'Select'
  },
  dataFormDialog: {
    addTitle: 'Add %{title}',
    updateTitle: 'Update %{title}',
    viewTitle: 'View %{title}'
  },
  actions: {
    save: 'Save',
    cancel: 'Cancel',
    confirm: 'Confirm'
  }
};

export default en_US;
