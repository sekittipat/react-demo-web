import CssBaseline from '@material-ui/core/CssBaseline';
import { withStyles } from '@material-ui/core/styles';
import React from 'react';

import TopBar from '../components/top-bar.jsx';

const styles = theme => ({
  appBarSpacer: theme.mixins.toolbar
});

class MainTemplate extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    const { classes } = this.props;

    return (
      <React.Fragment>
        <CssBaseline />
        <TopBar />
        <div className={classes.appBarSpacer} />
        {this.props.children}
      </React.Fragment>
    );
  }
}

export default withStyles(styles, { withTheme: true })(MainTemplate);
