import indigo from '@material-ui/core/colors/indigo';
import pink from '@material-ui/core/colors/pink';
import { createMuiTheme } from '@material-ui/core/styles';
import { ThemeProvider } from '@material-ui/styles';
import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import {
  i18nReducer,
  loadTranslations,
  setLocale,
  syncTranslationWithStore
} from 'react-redux-i18n';
import { BrowserRouter } from 'react-router-dom';
import { applyMiddleware, combineReducers, createStore } from 'redux';
import thunk from 'redux-thunk';

import { loginSuccess } from './actions/login-action.jsx';
import App from './app.jsx';
import { loginReducer } from './reducers/login-reducer.jsx';
import { sessionReducer } from './reducers/session-reducer.jsx';
import locales from './utils/locales.jsx';
import Logger from './utils/logger.jsx';
import { sessionManager } from './utils/session.jsx';

Logger.console.debug('App Context: ', window.appContext);

// set webpack 4 public path for support lazy loading
const jsBundlePath = window.appContext.contextPath + '/built/';
Logger.console.debug('JS Bundle Path: ', jsBundlePath);

__webpack_public_path__ = jsBundlePath;

// check last selected locale
let initLocale = sessionManager.getLocale() || 'th';

const store = createStore(
  combineReducers({
    login: loginReducer,
    session: sessionReducer,
    i18n: i18nReducer
  }),
  applyMiddleware(thunk)
);
// setup react-redux-i18n
syncTranslationWithStore(store);
store.dispatch(loadTranslations(locales));
store.dispatch(setLocale(initLocale));

// check login user session
if (sessionManager.isUserLoggedIn()) {
  Logger.console.debug('Already Logged In');
  let loginUser = sessionManager.getLoginUser();
  Logger.console.debug('Logged In User: ', loginUser);
  store.dispatch(loginSuccess(loginUser));
} else {
  Logger.console.debug('Not Logged In');
}

// custom theme
const theme = createMuiTheme({
  palette: {
    primary: indigo,
    secondary: pink
  }
});

ReactDOM.render(
  <BrowserRouter basename={window.appContext.contextPath}>
    <Provider store={store}>
      <ThemeProvider theme={theme}>
        <App />
      </ThemeProvider>
    </Provider>
  </BrowserRouter>,
  document.getElementById('react')
);
