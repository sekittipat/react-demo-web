import Grid from '@material-ui/core/Grid';
import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import React from 'react';
import { connect } from 'react-redux';

const i18n = require('react-redux-i18n').I18n;

const styles = theme => ({
  root: {
    margin: theme.spacing(3, 0)
  }
});
class LoadingPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  componentDidMount() {}

  render() {
    const { classes } = this.props;
    return (
      <Grid container className={classes.root}>
        <Grid item xs={12}>
          <Typography variant="h6" align="center">
            {i18n.t('loadingPage.pageLoading')}
          </Typography>
        </Grid>
      </Grid>
    );
  }
}

const mapStateToProps = state => {
  return {
    lang: state.i18n.locale
  };
};

const mapDispatchToProps = dispatch => ({});

const loadingPage = connect(
  mapStateToProps,
  mapDispatchToProps
)(LoadingPage);

export default withStyles(styles)(loadingPage);
