import Grid from '@material-ui/core/Grid';
import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import React from 'react';
import { connect } from 'react-redux';

const i18n = require('react-redux-i18n').I18n;

const styles = theme => ({
  root: {
    margin: theme.spacing(3, 0)
  }
});

class MainPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  componentDidMount() {}

  render() {
    const { classes } = this.props;
    return (
      <Grid container className={classes.root}>
        <Grid item xs={12}>
          <Typography variant="h5" align="center">
            {i18n.t('mainPage.welcome')},{' '}
            <strong>{this.props.login.user.username}</strong>
          </Typography>
        </Grid>
      </Grid>
    );
  }
}

const mapStateToProps = state => {
  return {
    login: state.login,
    lang: state.i18n.locale
  };
};

const mapDispatchToProps = dispatch => ({});

const mainPage = connect(
  mapStateToProps,
  mapDispatchToProps
)(MainPage);

export default withStyles(styles)(mainPage);
