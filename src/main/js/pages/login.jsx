import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import InputAdornment from '@material-ui/core/InputAdornment';
import Paper from '@material-ui/core/Paper';
import { withStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';
import AccountCircle from '@material-ui/icons/AccountCircle';
import Lock from '@material-ui/icons/Lock';
import axios from 'axios';
import React from 'react';
import { connect } from 'react-redux';

import { loginSuccess } from '../actions/login-action.jsx';
import { formFieldsOnChanged, formFieldsValidate } from '../utils/form.jsx';
import Logger from '../utils/logger.jsx';

const i18n = require('react-redux-i18n').I18n;

const styles = theme => ({
  root: {
    padding: theme.spacing(3, 2),
    margin: theme.spacing(3, 2)
  },
  button: {
    margin: theme.spacing(2, 0, 0, 0)
  }
});
class LoginPage extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      formFields: {
        username: {
          value: '',
          error: false,
          // errorMsg: '',
          required: true
        },
        password: {
          value: '',
          error: false,
          // errorMsg: '',
          required: true
        }
      },
      // errorMsg: '',
      formErrors: {
        invalidCredential: false
      },
      onSubmit: false
    };

    this.formFieldRefs = {
      username: React.createRef(),
      password: React.createRef()
    };
  }

  componentDidMount() {}

  render() {
    const { classes } = this.props;
    return (
      <Grid container alignItems="center" justify="center">
        <Grid item xs={12} sm={6} md={4}>
          <form onSubmit={this.btnSignin_onClick}>
            <Paper elevation={5} className={classes.root}>
              <Typography variant="h5">
                <strong>{i18n.t('loginPage.login')}</strong>
              </Typography>
              <TextField
                id="txtUsername"
                name="username"
                label={i18n.t('loginPage.username')}
                helperText={i18n.t('error.requiredField')}
                margin="normal"
                fullWidth
                autoFocus
                value={this.state.formFields.username.value}
                onChange={this.formFields_onChange}
                error={this.state.formFields.username.error}
                inputRef={this.formFieldRefs.username}
                disabled={this.state.onSubmit}
                InputProps={{
                  endAdornment: (
                    <InputAdornment position="end">
                      <AccountCircle />
                    </InputAdornment>
                  )
                }}
              />
              <TextField
                id="txtPassword"
                name="password"
                label={i18n.t('loginPage.password')}
                helperText={i18n.t('error.requiredField')}
                type="password"
                margin="normal"
                fullWidth
                value={this.state.formFields.password.value}
                onChange={this.formFields_onChange}
                error={this.state.formFields.password.error}
                inputRef={this.formFieldRefs.password}
                disabled={this.state.onSubmit}
                InputProps={{
                  endAdornment: (
                    <InputAdornment position="end">
                      <Lock />
                    </InputAdornment>
                  )
                }}
              />
              {this.renderFormErrors()}
              <Button
                variant="contained"
                color="primary"
                disabled={this.state.onSubmit}
                type="submit"
                fullWidth
                className={classes.button}
              >
                {i18n.t('loginPage.login')}
              </Button>
            </Paper>
          </form>
        </Grid>
      </Grid>
    );
  }

  formFields_onChange = event => {
    this.setState({
      formFields: formFieldsOnChanged(event, this.state.formFields)
    });
  };

  btnSignin_onClick = event => {
    event.preventDefault();

    this.setState({
      // errorMsg: '',
      formErrors: {
        invalidCredential: false
      },
      onSubmit: true
    });

    let results = formFieldsValidate(
      this.state.formFields,
      this.formFieldRefs,
      i18n.t('error.requiredField')
    );
    if (results.error) {
      this.setState({
        formFields: results.formFields,
        onSubmit: false
      });
      return;
    }

    let formData = new FormData(event.target);
    formData.append('grant_type', 'password');

    this.callLoginAPI(formData)
      .then(accessToken => this.callUserInfoAPI(accessToken))
      .then(user => {
        this.props.loginSuccess(user);
        // redirect to main page after login success
        this.props.history.push('/page/main');
      })
      .catch(error => {
        this.setState({
          // errorMsg: 'ชื่อผู้ใช้งาน หรือ รหัสผ่าน ไม่ถูกต้อง',
          formErrors: {
            invalidCredential: true
          },
          onSubmit: false
        });
        this.formFieldRefs.password.current.focus();
      });
  };

  callLoginAPI = formData => {
    return new Promise((resolve, reject) => {
      axios
        .post(window.appContext.contextPath + '/oauth/token', formData, {
          headers: { 'Content-Type': 'multipart/form-data' },
          auth: {
            username: 'clientId',
            password: 'clientSecret'
          }
        })
        .then(res => {
          let result = res.data;
          Logger.console.debug(
            'Login OAuth2 Response: ',
            JSON.stringify(result, null, 2)
          );

          resolve(result.access_token);
        })
        .catch(error => {
          if (error.response) {
            Logger.console.debug(
              'Login OAuth2 Error: ',
              JSON.stringify(error.response, null, 2)
            );
          }

          reject(error);
        });
    });
  };

  callUserInfoAPI = accessToken => {
    return new Promise((resolve, reject) => {
      axios
        .get(window.appContext.contextPath + '/api/users/me', {
          headers: { Authorization: `Bearer ${accessToken}` }
        })
        .then(res => {
          let result = res.data;
          Logger.console.debug(
            'Check User Info Response: ',
            JSON.stringify(result, null, 2)
          );

          resolve({
            username: result.username,
            token: `Bearer ${accessToken}`,
            roles: result.roles
          });
        })
        .catch(error => {
          if (error.response) {
            Logger.console.debug(
              'Check User Info Error: ',
              JSON.stringify(error.response, null, 2)
            );
          }

          reject(error);
        });
    });
  };

  renderFormErrors = () => {
    if (this.state.formErrors.invalidCredential) {
      return (
        <Typography variant="h6" color="error">
          {i18n.t('error.invalidCredential')}
        </Typography>
      );
    }

    return '';
  };
}

const mapStateToProps = state => {
  return {
    login: state.login,
    lang: state.i18n.locale
  };
};

const mapDispatchToProps = dispatch => ({
  loginSuccess: user => dispatch(loginSuccess(user))
});

const loginPage = connect(mapStateToProps, mapDispatchToProps)(LoginPage);

export default withStyles(styles)(loginPage);
