import { Grid, TextField } from '@material-ui/core';
import Breadcrumbs from '@material-ui/core/Breadcrumbs';
import Paper from '@material-ui/core/Paper';
import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Autocomplete from '@material-ui/lab/Autocomplete';
import React from 'react';
import { connect } from 'react-redux';

import TableSelectionPopup from '../../../components/form/table-selection-popup.jsx';
import TextFieldAutoComplete from '../../../components/form/textfield-autocomplete.jsx';
import TextFieldTablePopup from '../../../components/form/textfield-table-popup.jsx';
import PointGroupTableDialog from '../../../components/rtpms/point-group-table-dialog.jsx';
import Logger from '../../../utils/logger.jsx';

const i18n = require('react-redux-i18n').I18n;

const styles = theme => ({
  breadcrumb: {
    margin: theme.spacing(3, 2),
    padding: theme.spacing(1, 2)
  },
  paper: {
    margin: theme.spacing(3, 2),
    padding: theme.spacing(1, 2)
  }
});

class TextFieldTestPage extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      options: [
        { title: 'Movie 1' },
        { title: 'Movie 2' },
        { title: 'Movie 3' },
        { title: 'Movie 4' },
        { title: 'Movie 5' },
        { title: 'Movie 6' },
        { title: 'Movie 7' },
        { title: 'Movie 8' },
        { title: 'Movie 9' },
        { title: 'Movie 10' }
      ],
      value: {
        textField1: '',
        textField2: ''
      }
    };
  }

  componentDidMount() {}

  render() {
    const { classes, theme } = this.props;
    // Logger.console.debug('Style: ', classes, theme);
    return (
      <React.Fragment>
        <Paper elevation={4} className={classes.breadcrumb}>
          <Breadcrumbs aria-label="Breadcrumb">
            <Typography color="textSecondary">
              {i18n.t('menu.admin')}
            </Typography>
            <Typography color="textSecondary">
              {i18n.t('menu.componentTest')}
            </Typography>
            <Typography color="textPrimary">
              {i18n.t('menu.componentTest_Textfield')}
            </Typography>
          </Breadcrumbs>
        </Paper>
        <Paper elevation={4} className={classes.paper}>
          <Grid
            container
            spacing={3}
            direction="row"
            style={{ overflowY: 'auto' }} // fixed remove unexpected show vertical scrollbar
          >
            <Grid item xs={12}>
              <Typography variant="subtitle1" style={{ fontWeight: 'bold' }}>
                TexField Auto-Complete (Single Selection)
              </Typography>
            </Grid>
            <Grid item xs={12} md={4}>
              <Autocomplete
                // multiple
                id="single-selection"
                options={this.state.options}
                getOptionLabel={option => option.title}
                // defaultValue={this.state.options}
                renderInput={params => {
                  // Logger.console.debug('Params', params);
                  return (
                    <TextField
                      {...params}
                      variant="standard"
                      label="Single value"
                      placeholder="Favorites"
                    />
                  );
                }}
              />
            </Grid>
          </Grid>
        </Paper>
        <Paper elevation={4} className={classes.paper}>
          <Grid
            container
            spacing={3}
            direction="row"
            style={{ overflowY: 'auto' }} // fixed remove unexpected show vertical scrollbar
          >
            <Grid item xs={12}>
              <Typography variant="subtitle1" style={{ fontWeight: 'bold' }}>
                TexField Auto-Complete (Multiple Selection)
              </Typography>
            </Grid>
            <Grid item xs={12} md={4}>
              <Autocomplete
                multiple
                id="mutiple-selection"
                options={this.state.options}
                getOptionLabel={option => option.title}
                defaultValue={this.state.options}
                renderInput={params => {
                  // Logger.console.debug('Params', params);
                  return (
                    <TextField
                      {...params}
                      variant="standard"
                      label="Multiple values"
                      placeholder="Favorites"
                    />
                  );
                }}
              />
            </Grid>
          </Grid>
        </Paper>
        <Paper elevation={4} className={classes.paper}>
          <Grid
            container
            spacing={3}
            direction="row"
            style={{ overflowY: 'auto' }} // fixed remove unexpected show vertical scrollbar
          >
            <Grid item xs={12}>
              <Typography variant="subtitle1" style={{ fontWeight: 'bold' }}>
                TexField Auto-Complete (Multiple Selection with Table)
              </Typography>
            </Grid>
            <Grid item xs={12} md={4}>
              <TextFieldAutoComplete
                fullWidth
                label="Table values"
                name="textField1"
                value={this.state.value['textField1']}
                onChange={this.onTextFieldChange}
              />
            </Grid>
          </Grid>
        </Paper>

        <Paper elevation={4} className={classes.paper}>
          <Grid
            container
            spacing={3}
            direction="row"
            style={{ overflowY: 'auto' }} // fixed remove unexpected show vertical scrollbar
          >
            <Grid item xs={12}>
              <Typography variant="subtitle1" style={{ fontWeight: 'bold' }}>
                TexField Table Select (Single Selection)
              </Typography>
            </Grid>
            <Grid item xs={12} md={4}>
              <TextFieldTablePopup
                TextFieldProps={{
                  fullWidth: true,
                  label: 'Table values',
                  name: 'textField2',
                  // value: {this.state.value['textField2']}
                  // onChange={this.onTextFieldChange}
                  placeholder: '--- Select ---'
                }}
                renderPopup={popupProps => {
                  if (popupProps.isOpen()) {
                    return (
                      <PointGroupTableDialog
                        open={true}
                        multiSelect={false}
                        onClose={popupProps.onClose}
                        onSelect={popupProps.onSelect}
                      />
                    );
                  }

                  return '';
                }}
                onSelect={value => {
                  Logger.console.debug('Popup Selected Value', value);
                  // popupProps.setLabel('Test Set Label');
                  return `${value.pointGroupId} : ${value.pointGroupTHDesc}`;
                }}
              />
            </Grid>
          </Grid>
        </Paper>
        <Paper elevation={4} className={classes.paper}>
          <Grid
            container
            spacing={3}
            direction="row"
            style={{ overflowY: 'auto' }} // fixed remove unexpected show vertical scrollbar
          >
            <Grid item xs={12}>
              <Typography variant="subtitle1" style={{ fontWeight: 'bold' }}>
                Table Selection Popup
              </Typography>
            </Grid>
            <Grid item xs={12} md={4}>
              <TableSelectionPopup
                title={'Table values'}
                emptyText={'--- Please Select ---'}
                renderPopup={popupProps => {
                  if (popupProps.isOpen()) {
                    return (
                      <PointGroupTableDialog
                        open={true}
                        multiSelect={popupProps.isMultiSelect()}
                        onClose={popupProps.onClose}
                        onSelect={popupProps.onSelect}
                      />
                    );
                  }

                  return '';
                }}
                onSelect={values => {
                  Logger.console.debug('Popup Selected Values', values);

                  return values.map(value => ({
                    value: value.data.pointGroupId,
                    label: `${value.data.pointGroupId} : ${value.data.pointGroupTHDesc}`
                  }));
                }}
                multiSelect={true}
              />
            </Grid>
          </Grid>
        </Paper>
      </React.Fragment>
    );
  }

  onTextFieldChange = event => {
    let valueState = { ...this.state.value };
    valueState[event.target.name] = event.target.value;
    this.setState({
      value: valueState
    });
  };
}

const mapStateToProps = state => {
  return {
    login: state.login,
    lang: state.i18n.locale
  };
};

const mapDispatchToProps = dispatch => ({});

const textFieldTestPage = connect(
  mapStateToProps,
  mapDispatchToProps
)(TextFieldTestPage);

export default withStyles(styles, { withTheme: true })(textFieldTestPage);
