import Breadcrumbs from '@material-ui/core/Breadcrumbs';
import Paper from '@material-ui/core/Paper';
import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import React from 'react';
import { connect } from 'react-redux';

import RestAPIDataSourceTable from '../../../components/admin/data-source/restapi-data-source-table.jsx';
import Logger from '../../../utils/logger.jsx';

const i18n = require('react-redux-i18n').I18n;

const styles = theme => ({
  breadcrumb: {
    margin: theme.spacing(3, 2),
    padding: theme.spacing(1, 2)
  },
  tabBar: {
    margin: theme.spacing(3, 2, 0, 2)
  },
  tabContent: {
    margin: theme.spacing(0, 2, 3, 2),
    padding: theme.spacing(1, 2)
  },
  table: {
    margin: theme.spacing(3, 2)
  }
});

class RestAPIDataSourcePage extends React.Component {
  constructor(props) {
    super(props);

    this.state = {};
  }

  componentDidMount() {}

  render() {
    const { classes, theme } = this.props;
    Logger.console.debug('Style: ', classes, theme);
    return (
      <React.Fragment>
        <Paper elevation={4} className={classes.breadcrumb}>
          <Breadcrumbs aria-label="Breadcrumb">
            <Typography color="textSecondary">
              {i18n.t('menu.admin')}
            </Typography>
            <Typography color="textSecondary">
              {i18n.t('menu.dataSource')}
            </Typography>
            <Typography color="textPrimary">
              {i18n.t('menu.dataSourceRestAPI')}
            </Typography>
          </Breadcrumbs>
        </Paper>
        <Paper elevation={4} className={classes.table}>
          <RestAPIDataSourceTable />
        </Paper>
      </React.Fragment>
    );
  }
}

const mapStateToProps = state => {
  return {
    login: state.login,
    lang: state.i18n.locale
  };
};

const mapDispatchToProps = dispatch => ({});

const restAPIDataSourcePage = connect(
  mapStateToProps,
  mapDispatchToProps
)(RestAPIDataSourcePage);

export default withStyles(styles, { withTheme: true })(restAPIDataSourcePage);
