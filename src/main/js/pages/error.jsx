import React from 'react';
import { Redirect } from 'react-router-dom';

class ErrorPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  render() {
    return <Redirect to="/page/main" />;
  }
}

export default ErrorPage;
