import { Grid, Typography, withStyles } from '@material-ui/core';
import React from 'react';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';

import { logoutSuccess } from '../actions/login-action.jsx';

const i18n = require('react-redux-i18n').I18n;

const styles = theme => ({
  root: {
    margin: theme.spacing(3, 0)
  }
});

class LogoutPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    if (!this.props.login.isLoggedIn) {
      return <Redirect to="/page/login" />;
    }

    const { classes } = this.props;
    return (
      <Grid container className={classes.root}>
        <Grid item xs={12}>
          <Typography variant="h5" align="center">
            {i18n.t('logoutPage.logout')}
          </Typography>
        </Grid>
      </Grid>
    );
  }

  componentDidMount() {
    this.props.logoutSuccess();
  }
}

const mapStateToProps = state => {
  return {
    login: state.login
  };
};

const mapDispatchToProps = dispatch => ({
  logoutSuccess: () => dispatch(logoutSuccess())
});

const logoutPage = connect(mapStateToProps, mapDispatchToProps)(LogoutPage);

export default withStyles(styles)(logoutPage);
