import { SessionActionTypes } from '../actions/session-action.jsx';

// import Logger from '../utils/logger.jsx';

const sessionState = {
  remainingTimeSec: 0
};

export const sessionReducer = (state = sessionState, action) => {
  switch (action.type) {
    case SessionActionTypes.UPDATE_TIMER:
      // Logger.console.debug('Update Timer: ', action.remainingTimeSec);
      return Object.assign({}, state, {
        remainingTimeSec: action.remainingTimeSec
      });
    default:
      return state;
  }
};
