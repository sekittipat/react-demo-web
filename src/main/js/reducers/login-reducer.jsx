import { LoginActionTypes } from '../actions/login-action.jsx';
import Logger from '../utils/logger.jsx';

const loginState = {
  isLoggedIn: false,
  user: false
};

export const loginReducer = (state = loginState, action) => {
  switch (action.type) {
    case LoginActionTypes.LOGIN_SUCESS:
      Logger.console.debug('Login Success User:', action.user);
      return Object.assign({}, state, {
        isLoggedIn: true,
        user: action.user
      });
    case LoginActionTypes.LOGOUT_SUCESS:
      Logger.console.debug('Logout Success');
      return Object.assign({}, state, {
        isLoggedIn: false,
        user: false
      });
    default:
      return state;
  }
};
