import {
  Box,
  Collapse,
  Divider,
  Drawer,
  IconButton,
  List,
  ListItem,
  ListItemIcon,
  ListItemText,
  ListSubheader,
  Typography
} from '@material-ui/core';
import {
  ChevronLeft as ChevronLeftIcon,
  Code as CodeIcon,
  ExpandLess as ExpandLessIcon,
  ExpandMore as ExpandMoreIcon,
  Person as PersonIcon,
  Storage as StorageIcon
} from '@material-ui/icons';
import { withStyles } from '@material-ui/styles';
import React, { Component } from 'react';
import { Link } from 'react-router-dom';

import { Roles } from '../utils/constants.jsx';
import Permission from './permission.jsx';

const i18n = require('react-redux-i18n').I18n;

const styles = theme => ({
  drawerHeader: {
    display: 'flex',
    alignItems: 'center',
    padding: '0 8px',
    ...theme.mixins.toolbar,
    justifyContent: 'flex-start'
  },
  subMenuItem: {
    paddingLeft: theme.spacing(4)
  },
  itemIcon: {
    minWidth: 'unset',
    margin: theme.spacing(0, 1, 0, 0)
  }
});

class SideBar extends Component {
  constructor(props) {
    super(props);
    this.state = {
      menuOpen: {}
    };
  }
  render() {
    return this.renderSideMenu();
  }

  renderSideMenu = () => {
    const { classes } = this.props;
    const adminMenus = [
      {
        label: i18n.t('menu.user'),
        path: '/page/admin/user',
        icon: <PersonIcon />
      },
      {
        label: i18n.t('menu.dataSource'),
        key: 'dataSource',
        icon: <StorageIcon />,
        subMenus: [
          {
            path: '/page/admin/dataSource/database',
            label: i18n.t('menu.dataSourceDB')
          },
          {
            path: '/page/admin/dataSource/restAPI',
            label: i18n.t('menu.dataSourceRestAPI')
          }
        ]
      },
      {
        label: i18n.t('menu.componentTest'),
        key: 'componentTest',
        icon: <CodeIcon />,
        subMenus: [
          {
            path: '/page/admin/componentTest/textfield',
            label: i18n.t('menu.componentTest_Textfield')
          }
        ]
      }
    ];
    const mainMenus = [
      {
        label: 'Home',
        path: '/page/main'
      },
      {
        label: i18n.t('menu.lineAlert'),
        key: 'lineAlert',
        subMenus: [
          {
            path: '/page/lineAlert/lineAlertJob',
            label: i18n.t('menu.lineAlertJob')
          }
          // {
          //   path: '/page/menu/menu1',
          //   label: i18n.t('menu.lineAlertJobDetail')
          // }
        ]
      },
      {
        label: i18n.t('menu.rtpms'),
        key: 'rtpms',
        subMenus: [
          {
            path: '/page/rtpms/condition',
            label: i18n.t('menu.rtpmsCondition')
          },
          {
            path: '/page/rtpms/pointGroup',
            label: i18n.t('menu.rtpmsPointGroup')
          }
        ]
      }
    ];
    return (
      <Drawer anchor="left" open={this.props.open} onClose={this.props.onClose}>
        {/* set side menu width */}
        <div style={{ width: '250px' }}>
          {/* Side Menu Header */}
          <div className={classes.drawerHeader}>
            <Box
              component="span"
              display="flex"
              flexDirection="column"
              flexGrow={1}
            >
              <Typography variant="subtitle1">
                <strong>{window.appContext.name}</strong>
              </Typography>
              <Typography variant="caption">
                {window.appContext.version}
              </Typography>
            </Box>
            <IconButton onClick={this.props.onClose}>
              <ChevronLeftIcon />
            </IconButton>
          </div>
          <Divider />
          {/* Admin Menu */}
          <Permission roles={[Roles.ROLE_ADMIN]}>
            <List
              subheader={
                <ListSubheader component="div" id="nested-list-subheader">
                  {i18n.t('menu.admin')}
                </ListSubheader>
              }
            >
              {this.renderSideMenuList(adminMenus)}
            </List>
            <Divider />
          </Permission>
          {/* Main Menu */}
          <List
            subheader={
              <ListSubheader component="div" id="nested-list-subheader">
                {i18n.t('menu.main')}
              </ListSubheader>
            }
          >
            {/* Main Menu */}
            {this.renderSideMenuList(mainMenus)}
          </List>
        </div>
      </Drawer>
    );
  };

  renderSideMenuList = menuList => {
    const { classes } = this.props;
    return menuList.map((v, i) => {
      if (v.subMenus && v.subMenus.length > 0) {
        return (
          <React.Fragment key={'menu' + i}>
            <ListItem button onClick={() => this.toggleMenu(v.key)}>
              {this.renderSideMenuIcon(v)}
              <ListItemText primary={v.label} />
              {this.state.menuOpen[v.key] ? (
                <ExpandLessIcon />
              ) : (
                <ExpandMoreIcon />
              )}
            </ListItem>
            <Collapse
              in={this.state.menuOpen[v.key]}
              timeout="auto"
              unmountOnExit
            >
              <List key={'menuList' + i} component="div" disablePadding>
                {v.subMenus.map((v2, i2) => (
                  <ListItem
                    key={'subMenu' + i + i2}
                    button
                    className={classes.subMenuItem}
                    component={Link}
                    to={v2.path}
                    onClick={this.props.onClose}
                  >
                    {this.renderSideMenuIcon(v2)}
                    <ListItemText primary={v2.label} />
                  </ListItem>
                ))}
              </List>
            </Collapse>
          </React.Fragment>
        );
      } else {
        return (
          <ListItem
            key={'menu' + i}
            button
            component={Link}
            to={v.path}
            onClick={this.props.onClose}
          >
            {this.renderSideMenuIcon(v)}
            <ListItemText primary={v.label} />
          </ListItem>
        );
      }
    });
  };

  renderSideMenuIcon = menuItem => {
    const { classes } = this.props;
    if (menuItem.icon) {
      return (
        <ListItemIcon className={classes.itemIcon}>
          {menuItem.icon}
        </ListItemIcon>
      );
    }

    return '';
  };

  toggleMenu = key => {
    let menuOpenState = Object.assign({}, this.state.menuOpen);
    menuOpenState[key] = !menuOpenState[key];
    this.setState({
      menuOpen: menuOpenState
    });
  };
}

export default withStyles(styles)(SideBar);
