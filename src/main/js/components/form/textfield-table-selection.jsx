import {
  Checkbox,
  Grid,
  IconButton,
  InputAdornment,
  Paper,
  Popover,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  TextField,
  Tooltip,
  withStyles
} from '@material-ui/core';
import { ExpandMore as ExpandMoreIcon } from '@material-ui/icons';
import React, { Component } from 'react';

import Logger from '../../utils/logger.jsx';

const styles = theme => ({});

class TextFieldTableSelection extends Component {
  constructor(props) {
    super(props);
    this.state = {
      anchorEl: null,
      options: Array.from({ length: 50 }).map((value, index) => {
        return { title: `Movie ${index}`, year: 1980 + index, checked: false };
      }),
      textFieldWidth: 300,
      anchorOriginVert: 'top',
      transformOriginVert: 'bottom'
    };
    this.textFieldRef = React.createRef();
  }

  componentDidMount() {
    this.setTextFieldWidth();
    window.addEventListener('resize', this.onWindowResize);
    window.addEventListener('scroll', this.onWindowScroll);
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.onWindowResize);
    window.removeEventListener('scroll', this.onWindowScroll);
  }

  setTextFieldWidth = () => {
    if (this.textFieldRef && this.textFieldRef.current) {
      Logger.console.debug(
        'On Resize::Text Field Ref Width: ',
        this.textFieldRef.current.offsetWidth
      );
      const textFieldWidth = this.textFieldRef.current.offsetWidth;
      this.setState({
        textFieldWidth: textFieldWidth
      });
    }
  };

  onWindowResize = () => {
    this.setTextFieldWidth();
  };

  onWindowScroll = () => {
    if (this.textFieldRef && this.textFieldRef.current) {
      if (this.textFieldRef.current.offsetTop - window.pageYOffset > 300) {
        this.setState({
          anchorOriginVert: 'top',
          transformOriginVert: 'bottom'
        });
      } else {
        this.setState({
          anchorOriginVert: 'bottom',
          transformOriginVert: 'top'
        });
      }
    }
  };

  render() {
    const classes = this.props;
    return (
      <React.Fragment>
        <TextField
          {...this.props}
          ref={this.textFieldRef}
          InputProps={{
            // startAdornment: (
            //   <Box display="inline" flexDirection="row">
            //     {Array.from({ length: 10 }).map((v, i) => (
            //       <Chip
            //         key={i}
            //         label={`option ${i}`}
            //         onDelete={() => {}}
            //         style={{ marginRight: '2px' }}
            //       />
            //     ))}
            //   </Box>
            // ),
            endAdornment: (
              <InputAdornment position="end">
                <Tooltip title={'Click to show'}>
                  <span>
                    <IconButton
                      aria-label="clear search fields"
                      onClick={this.openAutoCompletePopup}
                      className={''}
                    >
                      <ExpandMoreIcon />
                    </IconButton>
                  </span>
                </Tooltip>
              </InputAdornment>
            )
          }}
          InputLabelProps={{
            shrink: true
          }}
        />
        {/* <FormControl fullWidth>
          <InputLabel htmlFor="text-autocomplete" shrink>
            {this.props.label}
          </InputLabel>
          <Input
            type="text"
            innerRef={el => {
              Logger.console.debug('Set TextField Ref', el);
              this.textFieldRef = el;
            }}
            // ref={this.textFieldRef}
            value={this.props.value}
            onChange={this.props.onChange}
            endAdornment={
              <InputAdornment position="end">
                <Tooltip title={'Click to show'}>
                  <span>
                    <IconButton
                      aria-label="clear search fields"
                      onClick={this.openAutoComplete}
                      className={''}
                    >
                      <ExpandMoreIcon />
                    </IconButton>
                  </span>
                </Tooltip>
              </InputAdornment>
            }
          />
        </FormControl> */}
        {this.renderAutoCompletePopup()}
      </React.Fragment>
    );
  }

  openAutoCompletePopup = event => {
    this.setState({
      anchorEl: this.textFieldRef.current
    });
  };

  closeAutoCompletePopup = () => {
    this.setState({
      anchorEl: null
    });
  };

  renderAutoCompletePopup = () => {
    return (
      <Popover
        open={Boolean(this.state.anchorEl)}
        anchorEl={this.state.anchorEl}
        onClose={this.closeAutoCompletePopup}
        anchorOrigin={{
          vertical: this.state.anchorOriginVert,
          horizontal: 'left'
        }}
        transformOrigin={{
          vertical: this.state.transformOriginVert,
          horizontal: 'left'
        }}
        style={{
          maxHeight: '300px'
        }}
      >
        <Grid
          container
          spacing={3}
          direction="row"
          style={{ overflowY: 'auto' }} // fixed remove unexpected show vertical scrollbar
        >
          <Grid item xs={12}>
            {this.renderAutoCompleteTable()}
          </Grid>
        </Grid>
      </Popover>
    );
  };

  renderAutoCompleteTable = () => {
    return (
      <TableContainer component={Paper}>
        <Table
          // className={classes.table}
          size="small"
          aria-label="a dense table"
          style={{
            minWidth: this.state.textFieldWidth
          }}
        >
          <TableHead>
            <TableRow>
              <TableCell>{''}</TableCell>
              <TableCell>Movie Name</TableCell>
              <TableCell align="right">Year</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {this.state.options.map((row, index) => (
              <TableRow key={index}>
                <TableCell padding="checkbox">
                  <Checkbox
                    // indeterminate={true}
                    checked={row.checked}
                    onChange={event =>
                      this.onRowChecked(index, event.target.checked)
                    }
                    inputProps={{ 'aria-label': 'select all desserts' }}
                  />
                </TableCell>
                <TableCell component="th" scope="row">
                  {row.title}
                </TableCell>
                <TableCell align="right">{row.year}</TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
    );
  };

  onRowChecked = (index, checked) => {
    let optionsState = [...this.state.options];
    optionsState[index].checked = checked;
    this.setState({
      options: optionsState
    });
  };
}

export default withStyles(styles, { withTheme: true })(TextFieldTableSelection);
