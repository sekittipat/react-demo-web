import {
  Box,
  Chip,
  FormControl,
  FormHelperText,
  FormLabel,
  IconButton,
  Tooltip,
  Typography
} from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';
import {
  Close as CloseIcon,
  ExpandMore as ExpandMoreIcon
} from '@material-ui/icons';
import PropTypes from 'prop-types';
import React, { Component } from 'react';

import Logger from '../../utils/logger.jsx';

const styles = theme => ({
  wrapper: {
    display: 'flex',
    flexDirection: 'row',
    borderBottom: `1px solid ${theme.palette.grey[600]}`,
    alignItems: 'center'
  },
  selectionValues: {
    display: 'flex',
    flex: 1,
    justifyContent: 'flex-start',
    alignItems: 'center',
    flexWrap: 'wrap',
    '& > *': {
      margin: theme.spacing(0.5)
    }
  },
  actionButton: {
    display: 'flex',
    flex: '0 0 20px'
  },
  formLabel: {
    fontSize: '0.75rem'
  },
  emptyText: {
    color: theme.palette.grey[500],
    margin: 'unset'
  }
});

class TableSelectionPopup extends Component {
  constructor(props) {
    super(props);
    this.state = {
      open: false,
      values: { ...props.values },
      popup: {
        isOpen: () => {
          return this.state.open;
        },
        isMultiSelect: () => {
          return props.multiSelect || false;
        },
        onSelect: values => {
          this.onTableSelect(values);
        },
        onClose: () => {
          this.closeTablePopup();
        }
      }
    };
  }

  render() {
    const { classes } = this.props;
    return (
      <React.Fragment>
        <FormControl fullWidth error={this.props.error}>
          <FormLabel className={classes.formLabel}>
            {this.props.title}
          </FormLabel>
          <Box className={classes.wrapper}>
            <Box className={classes.selectionValues}>{this.renderValues()}</Box>
            <Box className={classes.actionButton}>
              <Tooltip title={'Clear'}>
                <span>
                  <IconButton
                    aria-label="clear button"
                    onClick={this.clearValues}
                    className={''}
                    size="small"
                    disabled={this.isEmptyValues()}
                  >
                    <CloseIcon />
                  </IconButton>
                </span>
              </Tooltip>
            </Box>
            <Box className={classes.actionButton}>
              <Tooltip title={'Open popup'}>
                <span>
                  <IconButton
                    aria-label="open table popup button"
                    onClick={this.openTablePopup}
                    className={''}
                    size="small"
                  >
                    <ExpandMoreIcon />
                  </IconButton>
                </span>
              </Tooltip>
            </Box>
          </Box>
          <FormHelperText>{this.props.message}</FormHelperText>
        </FormControl>
        {this.props.renderPopup(this.state.popup)}
      </React.Fragment>
    );
  }

  renderValues = () => {
    const { classes } = this.props;
    if (!this.isEmptyValues()) {
      return Object.entries(this.state.values).map(([value, label], i) => (
        <Chip
          key={i}
          // size="small"
          label={label}
          onDelete={() => this.deleteValue(value)}
          color="secondary"
        />
      ));
    } else {
      return (
        <Typography
          variant="body1"
          component="div"
          display="inline"
          className={classes.emptyText}
        >
          {this.props.emptyText || 'No Selection'}
        </Typography>
      );
    }
  };

  openTablePopup = () => {
    this.setState({
      open: true
    });
  };

  closeTablePopup = () => {
    this.setState({
      open: false
    });
  };

  onTableSelect = values => {
    Logger.console.debug('Table values: ', values);

    let valuesState = this.props.multiSelect ? { ...this.state.values } : {};
    let selectedValues = this.props.onSelect(values);
    selectedValues.map(selected => {
      valuesState[selected.value] = selected.label;
    });
    this.setState({
      open: false,
      values: valuesState
    });
  };

  deleteValue = value => {
    let valuesState = { ...this.state.values };
    if (valuesState[value]) {
      delete valuesState[value];
    }

    this.setState({
      values: valuesState
    });
  };

  isEmptyValues = () => {
    return Object.entries(this.state.values).length == 0;
  };

  clearValues = () => {
    this.setState({
      values: {}
    });
  };
}

TableSelectionPopup.propTypes = {
  title: PropTypes.string.isRequired,
  multiSelect: PropTypes.bool,
  values: PropTypes.object,
  renderPopup: PropTypes.func.isRequired,
  onSelect: PropTypes.func.isRequired,
  error: PropTypes.bool,
  message: PropTypes.string,
  emptyText: PropTypes.string
};

export default withStyles(styles, { withTheme: true })(TableSelectionPopup);
