import {
  IconButton,
  InputAdornment,
  TextField,
  Tooltip,
  withStyles
} from '@material-ui/core';
import { ExpandMore as ExpandMoreIcon } from '@material-ui/icons';
import PropTypes from 'prop-types';
import React, { Component } from 'react';

import Logger from '../../utils/logger.jsx';

// import PointGroupTableDialog from '../rtpms/point-group-table-dialog.jsx';

const styles = theme => ({});

class TextFieldTablePopup extends Component {
  constructor(props) {
    super(props);
    this.state = {
      open: false,
      value: '',
      popup: {
        isOpen: () => {
          return this.state.open;
        },
        onSelect: value => {
          this.onTableSelect(value);
        },
        onClose: () => {
          this.closeTablePopup();
        },
        setLabel: label => {
          this.setTextFieldLabel(label);
        }
      }
    };
    this.textFieldRef = React.createRef();
  }

  render() {
    const classes = this.props;
    return (
      <React.Fragment>
        <TextField
          {...this.props.TextFieldProps}
          value={this.state.value}
          ref={this.textFieldRef}
          InputProps={{
            endAdornment: (
              <InputAdornment position="end">
                <Tooltip title={'Click to show'}>
                  <span>
                    <IconButton
                      aria-label="open table popup"
                      onClick={this.openTablePopup}
                      className={''}
                    >
                      <ExpandMoreIcon />
                    </IconButton>
                  </span>
                </Tooltip>
              </InputAdornment>
            )
          }}
          InputLabelProps={{
            shrink: true
          }}
        />
        {this.props.renderPopup(this.state.popup)}
      </React.Fragment>
    );
  }

  openTablePopup = () => {
    this.setState({
      open: true
    });
  };

  closeTablePopup = () => {
    this.setState({
      open: false
    });
  };

  onTableSelect = value => {
    //TODO: Add support table multiple selection mode
    Logger.console.debug('Table value: ', value);
    // this.setState({
    //   open: false
    // });
    let label = this.props.onSelect(value, this.state.popup) || '';
    this.setState({
      open: false,
      value: label
    });
  };
}

TextFieldTablePopup.propTypes = {
  TextFieldProps: PropTypes.object,
  renderPopup: PropTypes.func,
  onSelect: PropTypes.func
};

export default withStyles(styles, { withTheme: true })(TextFieldTablePopup);
