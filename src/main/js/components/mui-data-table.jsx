import {
  Box,
  Button,
  Checkbox,
  CircularProgress,
  Divider,
  Grid,
  Icon,
  IconButton,
  InputAdornment,
  ListItemIcon,
  Menu,
  MenuItem,
  Paper,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TablePagination,
  TableRow,
  TableSortLabel,
  TextField,
  Toolbar,
  Tooltip,
  Typography
} from '@material-ui/core';
import { grey } from '@material-ui/core/colors';
import { makeStyles, withStyles } from '@material-ui/core/styles';
import {
  AddBox,
  ArrowDownward,
  ArrowUpward,
  Delete as DeleteIcon,
  DoneAll as DoneAllIcon,
  FilterList,
  FilterNone as FilterNoneIcon,
  FirstPage,
  HighlightOff,
  KeyboardArrowLeft,
  KeyboardArrowRight,
  LastPage,
  MoreVert,
  Refresh,
  Search
} from '@material-ui/icons';
import { Alert } from '@material-ui/lab';
import classNames from 'classnames';
import React, { Component, useState } from 'react';
import { connect } from 'react-redux';

import { NumberFormat } from '../utils/formatter.jsx';
import Logger from '../utils/logger.jsx';

const i18n = require('react-redux-i18n').I18n;

const _ = require('lodash');

const toolbarStyles = makeStyles(theme => ({
  root: {
    paddingLeft: theme.spacing(1),
    paddingRight: theme.spacing(1)
  },
  title: {
    flex: 1,
    alignSelf: 'center'
  },
  searchText: {
    flex: 1,
    alignSelf: 'center'
  },
  actionButton: {
    color: 'inherit',
    '&:hover': {
      color: theme.palette.secondary.main
    },
    padding: theme.spacing(1)
  },
  titleGrid: {
    padding: theme.spacing(1),
    justifyContent: 'flex-start',
    alignItems: 'center'
  },
  searchGrid: {
    padding: theme.spacing(1),
    justifyContent: 'center',
    alignItems: 'center'
  },
  actionGrid: {
    padding: theme.spacing(1),
    justifyContent: 'flex-end',
    alignItems: 'center'
  },
  selectActionGrid: {
    padding: theme.spacing(1, 1),
    justifyContent: 'flex-start',
    alignItems: 'center'
  }
}));

const MuiDataTableToolbar = props => {
  const classes = toolbarStyles();
  const [searchFieldsMenuRef, setSearchFieldsMenuRef] = useState(null);
  const [moreActionsMenuRef, setMoreActionsMenuRef] = useState(null);

  const closeSearchFieldsMenu = () => {
    setSearchFieldsMenuRef(null);
  };

  const openSearchFieldsMenu = event => {
    setSearchFieldsMenuRef(event.currentTarget);
  };

  const renderSearchFieldsMenu = () => {
    if (
      props.searchColumns != undefined &&
      Object.entries(props.searchColumns).length > 0
    ) {
      return Object.entries(props.searchColumns).map(
        ([field, column], index) => (
          <MenuItem
            key={index}
            onClick={event => props.toggleSearchColumns(field)}
          >
            <ListItemIcon>
              <Checkbox
                edge="start"
                checked={column.checked}
                tabIndex={-1}
                disableRipple
              />
            </ListItemIcon>
            <Typography variant="inherit">{column.title}</Typography>
          </MenuItem>
        )
      );
    } else {
      return (
        <MenuItem>
          <Typography variant="inherit">Not available</Typography>
        </MenuItem>
      );
    }
  };

  const closeMoreActionsMenu = () => {
    setMoreActionsMenuRef(null);
  };

  const openMoreActionsMenu = event => {
    setMoreActionsMenuRef(event.currentTarget);
  };

  const renderMoreActionsMenu = () => {
    let actionMenus = getTableActionMenus();
    if (props.actions != undefined && props.actions.length > 0) {
      let customMenus = props.actions.map((action, index) => (
        <MenuItem key={index} onClick={action.onClick}>
          <ListItemIcon>
            {action.checkbox ? (
              <Checkbox
                edge="start"
                checked={action.checked}
                tabIndex={-1}
                disableRipple
              />
            ) : action.icon ? (
              <Icon>{action.icon}</Icon>
            ) : (
              ''
            )}
          </ListItemIcon>
          <Typography variant="inherit">{action.title}</Typography>
        </MenuItem>
      ));

      if (actionMenus.length > 0 && customMenus.length > 0) {
        actionMenus.push(<Divider key="divider" />);
      }
      actionMenus = [...actionMenus, ...customMenus];
    }

    if (actionMenus.length > 0) {
      return actionMenus;
    } else {
      return (
        <MenuItem>
          <Typography variant="inherit">Empty</Typography>
        </MenuItem>
      );
    }
  };

  const getTableActionMenus = () => {
    let actionMenus = [];

    // Disable select/deselect/delete all features
    // if (props.multiSelect && props.enableSelectAll) {
    //   actionMenus.push(
    //     <MenuItem
    //       key="selectAll"
    //       onClick={event => props.toggleSelectAll(true)}
    //       disabled={props.totalRows == 0}
    //     >
    //       <ListItemIcon>
    //         <DoneAllIcon />
    //       </ListItemIcon>
    //       <Typography variant="inherit">
    //         {i18n.t('dataTable.selectAll')}
    //       </Typography>
    //     </MenuItem>
    //   );
    //   actionMenus.push(
    //     <MenuItem
    //       key="deselectAll"
    //       onClick={event => props.toggleSelectAll(false)}
    //       disabled={props.totalRows == 0}
    //     >
    //       <ListItemIcon>
    //         <FilterNoneIcon />
    //       </ListItemIcon>
    //       <Typography variant="inherit">
    //         {i18n.t('dataTable.deselectAll')}
    //       </Typography>
    //     </MenuItem>
    //   );
    //   if (props.editable) {
    //     actionMenus.push(
    //       <MenuItem
    //         key="deleteAll"
    //         onClick={event => props.onDeleteAll()}
    //         disabled={props.totalRows == 0 || props.totalSelected == 0}
    //       >
    //         <ListItemIcon>
    //           <DeleteIcon />
    //         </ListItemIcon>
    //         <Typography variant="inherit">
    //           {i18n.t('dataTable.deleteAll')}
    //         </Typography>
    //       </MenuItem>
    //     );
    //   }
    // }

    return actionMenus;
  };

  const renderTotalSelectedMessage = () => {
    if (props.selectAll || props.totalSelected > 0) {
      return (
        <Grid container item xs={12} className={classes.selectActionGrid}>
          <div style={{ width: '100%' }}>
            <Alert severity={props.selectAll ? 'warning' : 'info'}>
              <b>
                {NumberFormat.formatInteger(
                  props.selectAll ? props.totalRows : props.totalSelected
                )}
              </b>{' '}
              {i18n.t('dataTable.rowsSelected')}
            </Alert>
          </div>
        </Grid>
      );
    }

    return '';
  };

  return (
    <Toolbar disableGutters className={classNames(classes.root)}>
      <Grid container spacing={0}>
        <Grid container item md={3} xs={12} className={classes.titleGrid}>
          <Typography className={classes.title} variant="h6">
            {props.title}
          </Typography>
        </Grid>
        <Grid container item md={6} xs={12} className={classes.searchGrid}>
          <TextField
            placeholder="Search"
            InputProps={{
              startAdornment: (
                <InputAdornment position="start">
                  <Search />
                </InputAdornment>
              ),
              endAdornment: (
                <InputAdornment position="end">
                  <Tooltip title={i18n.t('dataTable.clear')}>
                    <span>
                      <IconButton
                        aria-label="clear search fields"
                        onClick={props.onClearSearchText}
                        className={classes.actionButton}
                        disabled={props.searchText == ''}
                      >
                        <HighlightOff />
                      </IconButton>
                    </span>
                  </Tooltip>
                  <Tooltip title={i18n.t('dataTable.searchColumns')}>
                    <IconButton
                      aria-label="toggle search fields"
                      onClick={openSearchFieldsMenu}
                      aria-haspopup="true"
                      className={classes.actionButton}
                    >
                      <FilterList />
                    </IconButton>
                  </Tooltip>
                </InputAdornment>
              )
            }}
            className={classes.searchText}
            onChange={props.onSearchTextChanged}
            value={props.searchText}
          />
          <Menu
            id="simple-menu"
            anchorEl={searchFieldsMenuRef}
            keepMounted
            open={Boolean(searchFieldsMenuRef)}
            onClose={closeSearchFieldsMenu}
            getContentAnchorEl={null}
            anchorOrigin={{
              vertical: 'bottom',
              horizontal: 'left'
            }}
            PaperProps={{
              style: {
                maxHeight: 48 * 4.5
                // width: 200
              }
            }}
          >
            {renderSearchFieldsMenu()}
          </Menu>
        </Grid>
        <Grid container item md={3} xs={12} className={classes.actionGrid}>
          <Tooltip title={i18n.t('dataTable.refresh')}>
            <span>
              <IconButton
                aria-label="refresh button"
                color="inherit"
                onClick={event => props.onRefresh(true)}
                className={classes.actionButton}
              >
                <Refresh />
              </IconButton>
            </span>
          </Tooltip>
          <Tooltip title={i18n.t('dataTable.add')}>
            <span>
              <IconButton
                aria-label="add button"
                color="inherit"
                onClick={props.onAdd}
                className={classes.actionButton}
                disabled={props.editable !== undefined && !props.editable}
              >
                <AddBox />
              </IconButton>
            </span>
          </Tooltip>
          <Tooltip title={i18n.t('dataTable.moreActions')}>
            <span>
              <IconButton
                aria-label="more button"
                color="inherit"
                onClick={openMoreActionsMenu}
                className={classes.actionButton}
              >
                <MoreVert />
              </IconButton>
            </span>
          </Tooltip>
          <Menu
            id="simple-menu"
            anchorEl={moreActionsMenuRef}
            keepMounted
            open={Boolean(moreActionsMenuRef)}
            onClose={closeMoreActionsMenu}
            getContentAnchorEl={null}
            anchorOrigin={{
              vertical: 'bottom',
              horizontal: 'right'
            }}
            PaperProps={{
              style: {
                maxHeight: 48 * 4.5
                // width: 200
              }
            }}
          >
            {renderMoreActionsMenu()}
          </Menu>
        </Grid>
        {/* Select all action grid */}
        {renderTotalSelectedMessage()}
      </Grid>
    </Toolbar>
  );
};

const tableActionStyles = makeStyles(theme => ({
  root: {
    flexShrink: 0
  },
  actionButton: {
    color: 'inherit',
    '&:hover': {
      color: theme.palette.secondary.main
    }
  }
}));

const MuiDataTableActions = props => {
  const classes = tableActionStyles();
  const { count, page, rowsPerPage, onChangePage } = props;

  const handleFirstPageButtonClick = event => {
    onChangePage(event, 0);
  };

  const handleBackButtonClick = event => {
    onChangePage(event, page - 1);
  };

  const handleNextButtonClick = event => {
    onChangePage(event, page + 1);
  };

  const handleLastPageButtonClick = event => {
    onChangePage(event, Math.max(0, Math.ceil(count / rowsPerPage) - 1));
  };

  return (
    <Box display="flex" className={classes.root}>
      <IconButton
        onClick={handleFirstPageButtonClick}
        disabled={page === 0}
        aria-label="first page"
        color="inherit"
        className={classes.actionButton}
      >
        <FirstPage />
      </IconButton>
      <IconButton
        onClick={handleBackButtonClick}
        disabled={page === 0}
        aria-label="previous page"
        color="inherit"
        className={classes.actionButton}
      >
        <KeyboardArrowLeft />
      </IconButton>
      <IconButton
        onClick={handleNextButtonClick}
        disabled={page >= Math.ceil(count / rowsPerPage) - 1}
        aria-label="next page"
        color="inherit"
        className={classes.actionButton}
      >
        <KeyboardArrowRight />
      </IconButton>
      <IconButton
        onClick={handleLastPageButtonClick}
        disabled={page >= Math.ceil(count / rowsPerPage) - 1}
        aria-label="last page"
        color="inherit"
        className={classes.actionButton}
      >
        <LastPage />
      </IconButton>
    </Box>
  );
};

const styles = theme => ({
  root: {
    width: '100%'
  },
  container: {
    minHeight: 400,
    maxHeight: 400
  },
  loadingWrapper: {
    position: 'relative'
  },
  loadingOverlayOn: {
    position: 'absolute' /* Sit on top of the page content */,
    width: '100%' /* Full width (cover the whole page) */,
    height: '100%' /* Full height (cover the whole page) */,
    top: 0,
    left: 0,
    backgroundColor: 'rgba(0,0,0,0.3)' /* Black background with opacity */,
    zIndex: 10 /* Specify a stack order in case you're using a different order for other elements */,
    cursor: 'not-allowed' /* Add a pointer on hover */,
    alignItems: 'center',
    justifyContent: 'center'
  },
  loadingOverlayOff: {
    display: 'none'
  },
  loadingText: {
    color: '#fff'
  },
  headerColumn: {
    backgroundColor: grey[200]
  },
  evenRow: {
    backgroundColor: grey[50]
  },
  actionButton: {
    color: 'inherit',
    '&:hover': {
      color: theme.palette.secondary.main
    }
  },
  emptyRow: {
    textAlign: 'center'
  }
});

const SEARCH_INTERVAL = 600; // search interval in MS
class MuiDataTable extends Component {
  constructor(props) {
    super(props);

    let searchColumns = {};
    let searchColumnList = [];
    let sortColumn = false;
    props.columns.map((column, index) => {
      // initial search column menu list
      if (column.searchable) {
        searchColumns[column.field] = {
          title: column.title,
          checked: true
        };
        searchColumnList.push({
          fieldName: column.field
        });
      }
      // initial default sort field
      if (column.defaultSort) {
        sortColumn = {
          fieldName: column.field,
          direction: column.defaultSort
        };
      }
    });
    Logger.console.debug('Initial searchColumns', searchColumns);
    Logger.console.debug('Initial searchColumnList', searchColumnList);
    Logger.console.debug('Initial sortColumn', sortColumn);

    this.state = {
      search: '',
      filters: [],
      page: 0,
      pageSize: 5,
      rows: [],
      totalRows: 0,
      loading: false,
      searchColumns: searchColumns,
      searchColumnList: searchColumnList,
      sortColumn: sortColumn,
      pageSelected: [],
      pageCheckAll: [],
      pageCheckSome: [],
      totalSelected: 0,
      selectAll: false
    };

    this.searchTimeout = false;
  }

  isMultiSelect = () => {
    return this.props.selectionMode === 'multiple';
  };

  isSingleSelect = () => {
    return this.props.selectionMode === 'single';
  };

  render() {
    const { classes, theme } = this.props;

    return (
      <Box className={classes.loadingWrapper}>
        {/* Loading icon and overlay */}
        <Box
          display="flex"
          className={
            this.state.loading
              ? classes.loadingOverlayOn
              : classes.loadingOverlayOff
          }
        >
          <CircularProgress color="secondary" />
        </Box>
        <Paper className={classes.root}>
          <MuiDataTableToolbar
            title={this.props.title}
            onRefresh={this.refreshData}
            onAdd={this.props.onAdd}
            actions={this.props.tableActions}
            searchColumns={this.state.searchColumns}
            toggleSearchColumns={field => this.toggleSearchColumns(field)}
            onSearchTextChanged={this.handleSearchTextChanged}
            searchInterval={this.props.searchInterval}
            searchText={this.state.search}
            onClearSearchText={this.handleClearSearchText}
            multiSelect={this.isMultiSelect()}
            // enableSelectAll={this.props.enableSelectAll}
            // selectAll={this.state.selectAll}
            // toggleSelectAll={this.toggleSelectAll}
            totalSelected={this.state.totalSelected}
            totalRows={this.state.totalRows}
            onDeleteAll={() => {}}
            singleSelect={this.isSingleSelect()}
            editable={this.props.editable}
          />
          <TableContainer className={classes.container}>
            <Table stickyHeader size="small">
              <TableHead>{this.renderHeader()}</TableHead>
              <TableBody>{this.renderBody()}</TableBody>
            </Table>
          </TableContainer>
          <TablePagination
            component="div"
            rowsPerPageOptions={[5, 10, 50]}
            count={this.state.totalRows}
            rowsPerPage={this.state.pageSize}
            page={this.state.page}
            onChangePage={this.handleChangePage}
            onChangeRowsPerPage={this.handleChangeRowsPerPage}
            ActionsComponent={MuiDataTableActions}
          />
        </Paper>
      </Box>
    );
  }

  toggleSearchColumns = field => {
    let searchColumnsState = Object.assign({}, this.state.searchColumns);
    searchColumnsState[field].checked = !this.state.searchColumns[field]
      .checked;
    let searchColumnListState = Object.entries(searchColumnsState)
      .filter(([field, column]) => column.checked)
      .map(([field, column]) => {
        return {
          fieldName: field
        };
      });

    Logger.console.debug('Updated searchColumns', searchColumnsState);
    Logger.console.debug('Updated searchColumnList', searchColumnListState);

    this.setState(
      {
        searchColumns: searchColumnsState,
        searchColumnList: searchColumnListState
      },
      () => {
        this.refreshData(true);
      }
    );
  };

  renderHeader = () => {
    const { classes } = this.props;
    return (
      <TableRow>
        {this.renderHeaderSelectColumn()}
        <TableCell
          key="actions"
          className={classes.headerColumn}
          align="center"
        >
          Actions
        </TableCell>
        {this.props.columns.map((column, index) => {
          if (column.sortable != undefined && column.sortable == false) {
            return (
              <TableCell key={index} className={classes.headerColumn}>
                {column.title}
              </TableCell>
            );
          } else {
            let defaultSort = null;
            if (
              this.state.sortColumn &&
              this.state.sortColumn.fieldName === column.field
            ) {
              defaultSort = this.state.sortColumn.direction;
            }
            return (
              <TableCell key={index} className={classes.headerColumn}>
                <TableSortLabel
                  active={defaultSort != null}
                  IconComponent={event => {
                    return defaultSort === 'asc' ? (
                      <ArrowDownward />
                    ) : defaultSort === 'desc' ? (
                      <ArrowUpward />
                    ) : (
                      ''
                    );
                  }}
                  onClick={() =>
                    this.toggleSortDirection(column.field, defaultSort)
                  }
                >
                  {column.title}
                </TableSortLabel>
              </TableCell>
            );
          }
        })}
      </TableRow>
    );
  };

  renderHeaderSelectColumn = () => {
    const { classes } = this.props;
    if (this.isMultiSelect()) {
      return (
        <TableCell padding="checkbox" className={classes.headerColumn}>
          <Checkbox
            indeterminate={this.isPageCheckSome()}
            checked={this.isPageCheckAll()}
            onChange={event => this.onPageCheckAll()}
            inputProps={{ 'aria-label': 'select all checkbox' }}
            disabled={this.state.rows.length === 0}
          />
        </TableCell>
      );
    } else if (this.isSingleSelect()) {
      return (
        <TableCell className={classes.headerColumn} align="center">
          {i18n.t('dataTable.select')}
        </TableCell>
      );
    }

    return <React.Fragment />;
  };

  toggleSortDirection = (fieldName, direction) => {
    this.setState(
      {
        sortColumn: {
          fieldName: fieldName,
          direction:
            direction == null ? 'asc' : direction === 'asc' ? 'desc' : 'asc'
        }
      },
      () => {
        this.refreshData(true);
      }
    );
  };

  isPageCheckAll = () => {
    return this.state.pageCheckAll[this.state.page] || false;
  };

  isPageCheckSome = () => {
    return this.state.pageCheckSome[this.state.page] || false;
  };

  onPageCheckAll = () => {
    let checked = this.state.pageCheckAll[this.state.page] || false;

    const {
      pageSelected,
      pageCheckAll,
      pageCheckSome,
      totalSelected,
      selectAll
    } = this.selectAllPageRows(!checked);

    this.setState({
      pageSelected: pageSelected,
      pageCheckAll: pageCheckAll,
      pageCheckSome: pageCheckSome,
      totalSelected: totalSelected,
      selectAll: selectAll
    });
  };

  selectAllPageRows = selected => {
    let pageSelectedState = [...this.state.pageSelected];
    let pageCheckAllState = [...this.state.pageCheckAll];
    let pageCheckSomeState = [...this.state.pageCheckSome];

    let pageRowsSelected = pageSelectedState[this.state.page] || [];

    // set checked state to true for all rows in current page and update total selected
    let totalSelected = this.state.totalSelected;
    Logger.console.debug(
      'Total Selected (before page checkall): ',
      totalSelected
    );

    // const { startRowIndex, endRowIndex } = this.getPageRowIndex(
    //   this.state.page,
    //   this.state.pageSize,
    //   this.state.totalRows
    // );
    // for (let i = startRowIndex; i <= endRowIndex; i++) {
    //   let rowSelected = pageSelected[i] || false;

    //   if (rowSelected !== selected) {
    //     pageSelected[i] = selected;
    //     totalSelected = selected ? totalSelected + 1 : totalSelected - 1;
    //   }
    // }

    for (let i = 0; i < this.state.rows.length; i++) {
      let rowSelected = pageRowsSelected[i] || false;

      if (rowSelected !== selected) {
        pageRowsSelected[i] = selected;
        totalSelected = selected ? totalSelected + 1 : totalSelected - 1;
      }
    }

    Logger.console.debug(
      'Total Selected (after page checkAll): ',
      totalSelected
    );
    let selectAll = this.state.totalRows === totalSelected;
    Logger.console.debug('Select All (after page checkAll): ', selectAll);

    pageSelectedState[this.state.page] = pageRowsSelected;
    pageCheckAllState[this.state.page] = selected;
    pageCheckSomeState[this.state.page] = false;

    return {
      pageSelected: pageSelectedState,
      pageCheckAll: pageCheckAllState,
      pageCheckSome: pageCheckSomeState,
      totalSelected: totalSelected,
      selectAll: selectAll
    };
  };

  renderBody = () => {
    const { classes } = this.props;

    if (this.state.rows.length > 0) {
      return this.state.rows.map((row, rowIndex) => {
        return (
          <TableRow
            key={rowIndex}
            className={classNames((rowIndex + 1) % 2 == 0 && classes.evenRow)}
          >
            {this.renderSelectColumn(rowIndex)}
            {this.renderActionsColumn(row)}
            {this.props.columns.map((column, colIndex) => {
              if (column.render && typeof column.render == 'function') {
                return (
                  <TableCell key={colIndex}>
                    {this.renderColumn(column.render(row))}
                  </TableCell>
                );
              } else {
                // extract column value support nested object by lodash
                let value = _.get(row, column.field, '-');
                if (column.lookup && column.lookup[value] !== undefined) {
                  // lookup field
                  return (
                    <TableCell key={colIndex}>
                      {this.renderColumn(column.lookup[value])}
                    </TableCell>
                  );
                } else {
                  // value field
                  return (
                    <TableCell key={colIndex}>
                      {this.renderColumn(value)}
                    </TableCell>
                  );
                }
              }
            })}
          </TableRow>
        );
      });
    } else {
      // render not found rows message with col span equals to total columns + action column + checkbox column
      let totalColumns =
        this.props.columns.length +
        (this.isMultiSelect() || this.isSingleSelect() ? 2 : 1);
      return (
        <TableRow>
          <TableCell colSpan={totalColumns} align="center">
            <Typography>
              {this.state.loading
                ? i18n.t('dataTable.loading')
                : i18n.t('dataTable.empty')}
            </Typography>
          </TableCell>
        </TableRow>
      );
    }
  };

  renderColumn = value => {
    return value != null ? value : '-';
  };

  renderSelectColumn = rowIndex => {
    if (this.isMultiSelect()) {
      return (
        <TableCell padding="checkbox">
          <Checkbox
            key={rowIndex}
            checked={this.isRowChecked(rowIndex)}
            onChange={event => this.onRowChecked(rowIndex)}
            inputProps={{ 'aria-label': 'select checkbox' }}
          />
        </TableCell>
      );
    } else if (this.isSingleSelect()) {
      return (
        <TableCell>
          <Button
            variant="contained"
            color="secondary"
            onClick={event => this.onRowSelected(rowIndex)}
          >
            {i18n.t('dataTable.select')}
          </Button>
        </TableCell>
      );
    }

    return <React.Fragment />;
  };

  onRowChecked = rowIndex => {
    // clone pageSelected, pageCheckAll, pageCheckSome state
    let pageSelectedState = [...this.state.pageSelected];
    let pageCheckAllState = [...this.state.pageCheckAll];
    let pageCheckSomeState = [...this.state.pageCheckSome];

    // initial pageSelected, pageCheckAll, pageCheckSome state
    let pageRowsSelected = pageSelectedState[this.state.page] || [];

    // update row index selected state and update total selected
    let totalSelected = this.state.totalSelected;
    Logger.console.debug('Total Selected (before row select): ', totalSelected);
    let currentRowSelected = pageRowsSelected[rowIndex] || false;
    // toggle row selected for current row index
    pageRowsSelected[rowIndex] = !currentRowSelected;
    // update total rows selected
    totalSelected = pageRowsSelected[rowIndex]
      ? totalSelected + 1
      : totalSelected - 1;
    Logger.console.debug('Total Selected (after row select): ', totalSelected);
    // update selectAll state to true if total selected == total rows
    let selectAll = totalSelected === this.state.totalRows;
    Logger.console.debug('Select All (after row select): ', selectAll);

    // check page check all or check some
    let pageRowsSelectedTotal = 0;
    // const { startRowIndex, endRowIndex } = this.getPageRowIndex(
    //   this.state.page,
    //   this.state.pageSize,
    //   this.state.totalRows
    // );

    // for (let i = startRowIndex; i <= endRowIndex; i++) {
    for (let i = 0; i < this.state.rows.length; i++) {
      let rowSelected = pageRowsSelected[i] || false;
      Logger.console.debug(
        `Row Selected:[Page: ${this.state.page}, Row: ${i}]`,
        rowSelected
      );
      if (rowSelected) {
        pageRowsSelectedTotal++;
      }
    }

    let pageRowsTotal = this.state.rows.length;
    let pageCheckAll = pageRowsSelectedTotal === pageRowsTotal;
    let pageCheckSome =
      pageRowsSelectedTotal > 0 && pageRowsSelectedTotal < pageRowsTotal;
    Logger.console.debug('Page Selected Rows: ', pageRowsSelectedTotal);
    Logger.console.debug('Page Total Rows: ', pageRowsTotal);
    Logger.console.debug(
      `Page Selected: [${this.state.page}]`,
      pageRowsSelected
    );

    // set page selected row index
    pageSelectedState[this.state.page] = pageRowsSelected;
    pageCheckAllState[this.state.page] = pageCheckAll;
    pageCheckSomeState[this.state.page] = pageCheckSome;

    this.setState({
      pageSelected: pageSelectedState,
      pageCheckAll: pageCheckAllState,
      pageCheckSome: pageCheckSomeState,
      totalSelected: totalSelected,
      selectAll: selectAll
    });
  };

  isRowChecked = rowIndex => {
    let pageSelectedState = [...this.state.pageSelected];
    if (
      pageSelectedState[this.state.page] &&
      pageSelectedState[this.state.page][rowIndex]
    ) {
      return pageSelectedState[this.state.page][rowIndex];
    }

    return false;
  };

  onRowSelected = rowIndex => {
    let rowSelected = { ...this.state.rows[rowIndex] };
    Logger.console.debug('Row Selected: ', rowSelected);
    this.props.onSelect([
      { data: rowSelected, row: rowIndex, page: this.state.page }
    ]);
  };

  getSelectedRows = () => {
    let pageSelectedState = [...this.state.pageSelected];
    let pageSelected = pageSelectedState[this.state.page] || [];

    let rowsSelected = [];
    for (let i = 0; i < this.state.rows.length; i++) {
      if (pageSelected[i]) {
        rowsSelected.push({
          data: this.state.rows[i],
          row: i,
          page: this.state.page
        });
      }
    }

    return rowsSelected;
  };

  renderActionsColumn = rowData => {
    const { classes } = this.props;
    return (
      <TableCell key="actions">
        <Box display="flex">
          {this.props.rowActions &&
            this.props.rowActions.map((action, index) => (
              <Tooltip key={index} title={action.tooltip}>
                <IconButton
                  key={index}
                  aria-label="action button"
                  color="inherit"
                  className={classes.actionButton}
                  onClick={event => action.onClick(event, rowData)}
                >
                  <Icon>{action.icon}</Icon>
                </IconButton>
              </Tooltip>
            ))}
          {this.props.editable ? (
            <React.Fragment>
              <Tooltip title={i18n.t('dataTable.edit')}>
                <IconButton
                  key="editAction"
                  aria-label="action button"
                  color="inherit"
                  className={classes.actionButton}
                  onClick={event => this.props.onEdit(event, rowData)}
                >
                  <Icon>edit</Icon>
                </IconButton>
              </Tooltip>
              <Tooltip title={i18n.t('dataTable.delete')}>
                <IconButton
                  key="deleteAction"
                  aria-label="action button"
                  color="inherit"
                  className={classes.actionButton}
                  onClick={event => this.props.onDelete(event, rowData)}
                >
                  <Icon>delete</Icon>
                </IconButton>
              </Tooltip>
            </React.Fragment>
          ) : (
            <Tooltip title={i18n.t('dataTable.view')}>
              <IconButton
                key="viewAction"
                aria-label="action button"
                color="inherit"
                className={classes.actionButton}
                onClick={event => this.props.onView(event, rowData)}
              >
                <Icon>visibility</Icon>
              </IconButton>
            </Tooltip>
          )}
        </Box>
      </TableCell>
    );
  };

  componentDidMount() {
    this.refreshData();
  }

  refreshData = (resetPage, resetSelected) => {
    Logger.console.debug(
      `Refresh Data [resetPage: ${resetPage}, resetSelected: ${resetSelected}] `
    );
    if (this.props.data !== undefined) {
      if (typeof this.props.data == 'function') {
        Logger.console.debug('Call promise function for get remote data');
        this.setState(
          {
            loading: true
          },
          () => {
            // setTimeout(this.searchData, 2000); /* For test only */
            this.searchData(resetPage, resetSelected);
          }
        );
      } else if (Array.isArray(this.props.data) && this.props.data.length > 0) {
        Logger.console.debug('Set local data');
        let page = resetPage === true ? 0 : this.state.page;
        let totalRows = this.props.data.length;

        const { startRowIndex, endRowIndex } = this.getPageRowsRange(
          page,
          this.state.pageSize,
          totalRows
        );

        let pageRows = this.props.data.slice(startRowIndex, endRowIndex);
        let result = {
          data: pageRows,
          page: page,
          totalCount: totalRows
        };

        this.initialPageState(result, resetPage, resetSelected);
      }
    } else {
      Logger.console.debug('Data props is undefined');
    }
  };

  searchData = (resetPage, resetSelected) => {
    let query = {
      search: this.state.search,
      page: resetPage === true ? 0 : this.state.page,
      pageSize: this.state.pageSize,
      filters: this.state.filters,
      searchColumns: this.state.searchColumnList
    };
    if (this.state.sortColumn) {
      query.orderBy = this.state.sortColumn.fieldName;
      query.orderDirection = this.state.sortColumn.direction;
    }
    this.props
      .data(query)
      .then(result => {
        Logger.console.debug('onSearch :: Result', result);
        // initial pageSelected state
        this.initialPageState(result, resetPage, resetSelected);
      })
      .catch(error => {
        Logger.console.error('onSearch :: Error', error);
      })
      .finally(() => {
        this.setState({
          loading: false
        });
      });
  };

  handleChangePage = (event, newPage) => {
    this.setState(
      {
        page: newPage
      },
      () => {
        this.refreshData(false, true);
      }
    );
  };

  handleChangeRowsPerPage = event => {
    this.setState(
      {
        page: 0,
        pageSize: event.target.value
      },
      () => {
        this.refreshData(true);
      }
    );
  };

  handleSearchTextChanged = event => {
    Logger.console.debug('Search Text: ', event.target.value);
    this.setState(
      {
        search: event.target.value
      },
      () => {
        if (this.searchTimeout) {
          clearTimeout(this.searchTimeout);
        }
        this.searchTimeout = setTimeout(() => {
          this.refreshData(true);
        }, this.props.searchInterval || SEARCH_INTERVAL);
      }
    );
  };

  handleClearSearchText = () => {
    this.setState(
      {
        search: ''
      },
      () => {
        this.refreshData(true);
      }
    );
  };

  initialPageState = (result, resetPage, resetSelected) => {
    let pageSelectedState = [...this.state.pageSelected];
    let pageCheckAllState = [...this.state.pageCheckAll];
    let pageCheckSomeState = [...this.state.pageCheckSome];
    let totalSelected = this.state.totalSelected;
    let selectAll = this.state.selectAll;

    if (resetPage || resetSelected) {
      pageSelectedState = [];
      pageCheckAllState = [];
      pageCheckSomeState = [];
      totalSelected = 0;
      selectAll = false;
    } else {
      if (this.state.selectAll) {
        let pageRowsSelected = pageSelectedState[result.page] || [];
        // const { startRowIndex, endRowIndex } = this.getPageRowIndex(
        //   result.page,
        //   this.state.pageSize,
        //   result.totalCount
        // );
        // set all row selected in current page
        // for (let i = startRowIndex; i <= endRowIndex; i++) {
        for (let i = 0; i < result.data.length; i++) {
          pageRowsSelected[i] = true;
        }
        pageSelectedState[result.page] = pageRowsSelected;
        pageCheckAllState[result.page] = true;
        pageCheckSomeState[result.page] = false;
      }
    }

    this.setState({
      rows: result.data,
      page: result.page,
      totalRows: result.totalCount,
      pageSelected: pageSelectedState,
      pageCheckAll: pageCheckAllState,
      pageCheckSome: pageCheckSomeState,
      totalSelected: totalSelected,
      selectAll: selectAll
    });
  };

  getPageRowsRange = (page, pageSize, totalRows) => {
    if (totalRows > 0) {
      let startIndex = page * pageSize;
      let endIndex = startIndex + pageSize - 1;
      if (endIndex > totalRows - 1) {
        endIndex = totalRows - 1;
      }

      return {
        startRowIndex: startIndex,
        endRowIndex: endIndex
      };
    } else {
      return {
        startRowIndex: 0,
        endRowIndex: 0
      };
    }
  };

  toggleSelectAll = selectAll => {
    //update state page selected rows for current page
    if (this.state.selectAll !== selectAll) {
      if (selectAll) {
        const {
          pageSelected,
          pageCheckAll,
          pageCheckSome,
          totalSelected
        } = this.selectAllPageRows(selectAll);
        this.setState({
          pageSelected: pageSelected,
          pageCheckAll: pageCheckAll,
          pageCheckSome: pageCheckSome,
          totalSelected: totalSelected,
          selectAll: true
        });
      } else {
        // Deselect all
        this.setState({
          pageSelected: [],
          pageCheckAll: [],
          pageCheckSome: [],
          totalSelected: 0,
          selectAll: false
        });
      }
    }
  };
}

const mapStateToProps = state => {
  return {
    login: state.login,
    lang: state.i18n.locale
  };
};

const mapDispatchToProps = dispatch => ({});

const MuiDataTableRedux = connect(mapStateToProps, mapDispatchToProps, null, {
  forwardRef: true
})(MuiDataTable);

const MuiDataTableReduxWithStyles = withStyles(styles, { withTheme: true })(
  MuiDataTableRedux
);

export default React.forwardRef((props, ref) => (
  <MuiDataTableReduxWithStyles {...props} ref={ref} />
));
