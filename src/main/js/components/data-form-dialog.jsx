import {
  AppBar,
  Button,
  Dialog,
  DialogContent,
  Grid,
  IconButton,
  Paper,
  Slide,
  Tab,
  Tabs,
  TextField,
  Toolbar,
  Typography,
  withStyles
} from '@material-ui/core';
import CloseIcon from '@material-ui/icons/Close';
import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { connect } from 'react-redux';

import Logger from '../utils/logger.jsx';

const i18n = require('react-redux-i18n').I18n;

const _ = require('lodash');

const moment = require('moment');

const styles = theme => ({
  appBar: {
    backgroundColor: theme.palette.primary.main
  },
  appBarNested: {
    backgroundColor: theme.palette.primary.dark
  },
  appBarSpacer: theme.mixins.toolbar,
  title: {
    marginLeft: theme.spacing(2),
    flex: 1,
    [theme.breakpoints.down('xs')]: {
      fontSize: theme.typography.subtitle1.fontSize
    }
  },
  childTabs: {
    margin: theme.spacing(3, 3)
  }
});

const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />;
});

class DataFormDialog extends Component {
  constructor(props) {
    super(props);

    this.state = this.intialState(props);

    this.validator = {
      userId: (value, formData) => {
        Logger.console.debug('Validate User ID: ' + value);
        let valid = /^[a-z][a-z0-9]*$/.test(value);
        Logger.console.debug('Valid: ', valid);
        return {
          error: !valid,
          message: !valid
            ? 'User ID must contain only lower charactor and digit'
            : ''
        };
      },
      password: (value, formData) => {
        Logger.console.debug('Validate Password: ' + value);
        let valid = /^[^\s]{6,}$/.test(value);
        Logger.console.debug('Valid: ', valid);
        return {
          error: !valid,
          message: !valid
            ? 'Password minimun length is 6 and not allow space'
            : ''
        };
      },
      email: (value, formData) => {
        Logger.console.debug('Validate E-mail: ' + value);
        let valid = /^[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,64}$/.test(
          value
        );
        Logger.console.debug('Valid: ', valid);
        return {
          error: !valid,
          message: !valid ? 'E-mail is invalid format' : ''
        };
      },
      date: (value, formData) => {
        let valid = moment(value).isValid();
        return {
          error: !valid,
          message: !valid ? 'Date is invalid format' : ''
        };
      }
    };

    // Left / Up / Right / Down Arrow, Backspace, Delete keys
    this.skipKeyCodes = [37, 38, 39, 40, 8, 46];
  }

  intialState = props => {
    Logger.console.debug('Initial State');
    let editMode = false;
    let titleMsgKey = 'dataFormDialog.addTitle';
    let readOnly = false;
    if (props.rowData !== undefined) {
      editMode = true;
      readOnly = !this.props.editable;
      titleMsgKey = readOnly
        ? 'dataFormDialog.viewTitle'
        : 'dataFormDialog.updateTitle';
    }

    Logger.console.debug('Row Data: ', props.rowData);
    Logger.console.debug('Edit Mode: ', editMode);
    Logger.console.debug('Read Only: ', readOnly);

    let formData = {};
    let formFields = {};
    let lookupFields = {};

    props.columns.map((column, i) => {
      if (column.editable == 'none') {
        // skip populate column state
        return false;
      }

      let disabled = !(
        (column.editable == 'onAdd' && !editMode) ||
        column.editable == 'always'
      );

      let fieldValue = '';

      // initial form data state (only editable field)
      if (editMode) {
        // support get/set nested object value
        // fieldValue = props.rowData[column.field] || '';
        fieldValue = _.get(props.rowData, column.field, '') || '';
        // update mode and editable == 'always' or primary key column
        // formData[column.field] = fieldValue;
      } else {
        fieldValue = column.defaultValue || '';
        // formData[column.field] = fieldValue;
      }
      _.set(formData, column.field, fieldValue);

      // initial lookup fields
      let lookup = false;
      if (column.lookup && typeof column.lookup == 'function') {
        lookupFields[column.field] = {};
        lookup = {
          ready: true,
          items: []
        };

        let lookupResult = column.lookup(props.rowData);
        if (Array.isArray(lookupResult)) {
          Logger.console.debug('Local lookup field: ', column.field);
          lookupFields[column.field].local = column;
          lookup.items = lookupResult;
        } else if (typeof lookupResult == 'function') {
          Logger.console.debug('Remote lookup field: ', column.field);
          lookupFields[column.field].remote = column;
        }
      }

      // intial form field state
      formFields[column.field] = {
        title: column.title,
        value: fieldValue,
        lookup: lookup,
        error: false,
        message: '',
        disabled: disabled,
        editMode: editMode,
        readOnly: readOnly,
        keyCode: false,
        ref: React.createRef(),
        onChange: (value, otherValues) => {
          this.onFieldsValueChange(column, value, otherValues);
        },
        onKeyDown: event => {
          this.onFieldsKeyDown(column, event);
        }
      };
    });

    Logger.console.debug('Initial Form Fields: ', formFields);
    Logger.console.debug('Initial Form Data: ', formData);
    Logger.console.debug('Initial Lookup Fields: ', lookupFields);

    return {
      editMode: editMode,
      titleMsgKey: titleMsgKey,
      formFields: formFields,
      formData: formData,
      lookupFields: lookupFields,
      childTabIndex: 0
    };
  };

  static getDerivedStateFromProps(newProps, currentState) {
    // ****************
    // sort columns
    // ****************
    let sortedColumns = DataFormDialog.sortByOrder(newProps.columns);

    // ********************
    // sort childs tab
    // ********************
    let sortedChilds = DataFormDialog.sortByOrder(newProps.childs);

    return {
      sortedColumns: sortedColumns,
      sortedChilds: sortedChilds
    };
  }

  static sortByOrder(objects) {
    let sortedObjects = [];
    if (objects && objects.length > 0) {
      sortedObjects = [...objects];
      // find max defined column order
      let maxOrder = Math.max.apply(
        Math,
        sortedObjects.map(column =>
          column.order == undefined ? 0 : column.order
        )
      );
      // increase by 1 from max defined column order
      maxOrder++;
      // Logger.console.debug('Initial Column Order (Max + 1): ', columnOrder);
      sortedObjects.sort((second, first) => {
        // Logger.console.debug(
        //   'Compare Sort (step 1): ',
        //   columnOrder,
        //   first,
        //   second
        // );
        if (first.order == undefined) {
          first.order = maxOrder++; // increment column order after assign to column property
        }
        if (second.order == undefined) {
          second.order = maxOrder++; // increment column order after assign to column property
        }
        // Logger.console.debug(
        //   'Compare Sort (step 2): ',
        //   columnOrder,
        //   first,
        //   second
        // );
        return second.order == first.order
          ? 0
          : second.order > first.order
          ? 1
          : -1;
      });
    }

    return sortedObjects;
  }

  componentDidMount() {
    // intial all lookup remote data
    let lookupFieldNames = Object.entries(this.state.lookupFields)
      .filter(([fieldName, lookup]) => lookup && lookup.remote)
      .map(([fieldName, lookup]) => fieldName);
    this.loadAllRemoteLookupData(lookupFieldNames);
  }

  onFieldsValueChange = (column, value, otherValues) => {
    Logger.console.debug(`onFieldsValueChange: [${column.field} = ${value}]`);
    let formFieldsState = Object.assign({}, this.state.formFields);
    let formDataState = Object.assign({}, this.state.formData);

    if (formFieldsState[column.field].value === value) {
      Logger.console.debug('Field value not changed');
      return;
    }

    //  skip backspace, delete and arrow keys
    let valid = {
      error: false,
      message: ''
    };
    let keyCode = formFieldsState[column.field].keyCode;
    Logger.console.debug('Key Code: ', keyCode);
    if (!this.skipKeyCodes.includes(keyCode)) {
      valid = this.validateField(column, value, formDataState);
    }

    formFieldsState[column.field].value = value;
    formFieldsState[column.field].error = valid.error;
    formFieldsState[column.field].message = valid.message;
    // formDataState[column.field] = value;
    _.set(formDataState, column.field, value);

    // update related other form field value if specified
    if (otherValues !== undefined && Array.isArray(otherValues)) {
      otherValues.map(otherField => {
        formFieldsState[otherField.name].value = otherField.value;
        // get lookup field data local
        formFieldsState[otherField.name].lookup = this.getLocalLookupData(
          otherField.name,
          formDataState
        );

        // formDataState[otherField.name] = otherField.value;
        _.set(formDataState, otherField.name, otherField.value);
      });
    }

    Logger.console.debug('Fields State: ', formFieldsState);
    Logger.console.debug('Form Data: ', formDataState);

    this.setState(
      {
        formFields: formFieldsState,
        formData: formDataState
      },
      () => {
        // reload all other fields lookup remote data
        if (otherValues !== undefined && Array.isArray(otherValues)) {
          let otherFieldNames = otherValues.map(otherField => otherField.name);
          this.loadAllRemoteLookupData(otherFieldNames);
        }
      }
    );
  };

  onFieldsKeyDown = (column, event) => {
    let keyCode = event.keyCode || event.which;
    Logger.console.debug(
      'onKeyDown: ' + column.field + ', key code: ' + keyCode
    );
    let formFieldsState = Object.assign({}, this.state.formFields);
    formFieldsState[column.field].keyCode = keyCode;
    this.setState({
      formFields: formFieldsState
    });
  };

  loadAllRemoteLookupData = fieldNames => {
    fieldNames.map(fieldName => {
      if (this.state.lookupFields[fieldName]) {
        let lookupField = this.state.lookupFields[fieldName];
        if (lookupField && lookupField.remote) {
          this.loadRemoteLookupData(lookupField.remote);
        }
      }
    });
  };

  loadRemoteLookupData = column => {
    Logger.console.debug('Load remote lookup data: ', column);
    let callbackFunction = column.lookup(this.state.formData);
    if (typeof callbackFunction == 'function') {
      let callbackPromise = callbackFunction();
      if (callbackPromise instanceof Promise) {
        this.setLookupData(column.field, false, [], () => {
          callbackPromise
            .then(lookupItems => {
              this.setLookupData(column.field, true, lookupItems);
            })
            .catch(error => {
              //TODO: handle remote lookup data error
              this.setLookupData(column.field, true, []);
            });
        });
      } else {
        Logger.console.debug('Remote lookup promise not defined');
      }
    } else {
      Logger.console.debug('Remote lookup function not defined');
    }
  };

  setLookupData = (fieldName, ready, items, callback) => {
    let formFieldsState = Object.assign({}, this.state.formFields);
    formFieldsState[fieldName].lookup = {
      ready: ready,
      items: items
    };
    this.setState(
      {
        formFields: formFieldsState
      },
      () => {
        if (callback != undefined && typeof callback == 'function') {
          callback();
        }
      }
    );
  };

  getLocalLookupData = (fieldName, formData) => {
    let lookupData = false;
    if (this.state.lookupFields[fieldName]) {
      lookupData = {
        ready: true,
        items: []
      };
      let lookupField = this.state.lookupFields[fieldName];
      if (lookupField && lookupField.local) {
        Logger.console.debug('Get lookup local data: ', fieldName);
        let lookupItems = lookupField.local.lookup(formData);
        if (Array.isArray(lookupItems)) {
          lookupData.items = lookupItems;
        }
      }
    }

    return lookupData;
  };

  render() {
    const { classes } = this.props;

    return (
      <Dialog
        open={this.props.open}
        onClose={this.props.onClose}
        // aria-labelledby="form-dialog-title"
        fullScreen
        TransitionComponent={Transition}
        disableBackdropClick
        disableEscapeKeyDown
        // scroll="body"
      >
        <form onSubmit={this.handleSubmit}>
          {/* <AppBar className={classes.appBar}> */}
          <AppBar
            className={
              this.props.nested ? classes.appBarNested : classes.appBar
            }
          >
            <Toolbar>
              <IconButton
                edge="start"
                color="inherit"
                onClick={this.props.onClose}
                aria-label="close"
              >
                <CloseIcon />
              </IconButton>
              <Typography variant="h6" className={classes.title}>
                {i18n.t(this.state.titleMsgKey, {
                  title: this.props.title
                })}
              </Typography>
              {this.renderSaveButton()}
              <Button color="inherit" onClick={this.props.onClose}>
                {i18n.t('actions.cancel')}
              </Button>
            </Toolbar>
          </AppBar>
          <DialogContent>
            <div className={classes.appBarSpacer} />
            <Grid
              container
              spacing={3}
              direction="row"
              style={{ overflowY: 'auto' }} // fixed remove unexpected show vertical scrollbar
            >
              {/* {this.state.sortedColumns.map((v, i) => this.renderField(v, i))} */}
              {this.renderFields()}
            </Grid>
          </DialogContent>
        </form>
        {this.renderChilds()}
      </Dialog>
    );
  }

  renderSaveButton = () => {
    if (this.props.editable) {
      return (
        <Button
          type="submit"
          color="inherit"
          // onClick={this.props.onClose}
        >
          {i18n.t('actions.save')}
        </Button>
      );
    }
    return '';
  };

  renderFields = () => {
    // support set layout columns per row (valid value can be 12, 6, 4, 3, 2, 1) default is 3
    const columnsPerRow = this.props.columnsPerRow || 3;
    const colspanDefault = 12 / columnsPerRow;

    return this.state.sortedColumns.map((column, index) => {
      if (column.editable == 'none') {
        // skip render field
        return '';
      }

      let fieldState = this.state.formFields[column.field];

      if (column && column.editable !== undefined) {
        // check colspan
        let colspan = colspanDefault;
        if (column.colspan && column.colspan > 1) {
          colspan = colspanDefault * column.colspan;
        }
        let formEditable = this.props.editable;
        if (
          formEditable &&
          column.editable !== 'never' &&
          column.render !== undefined &&
          typeof column.render == 'function'
        ) {
          // render edit components

          return (
            <Grid key={index} item xs={12} md={colspan}>
              {/* {column.render(fieldState, index)} */}
              {column.render(fieldState, index, this.state.formData)}
            </Grid>
          );
        } else {
          // render read-only field
          let value = '';
          if (
            column.format !== undefined &&
            typeof column.format == 'function'
          ) {
            value = column.format(fieldState.value, this.props.rowData);
            // } else if (column.lookup && column.lookup[fieldState.value]) {
            //   value = column.lookup[fieldState.value];
          } else {
            value = fieldState.value;
          }

          return (
            <Grid key={index} item xs={12} md={colspan}>
              <TextField
                key={index}
                margin="normal"
                label={column.title}
                fullWidth
                value={
                  // column.format !== undefined &&
                  // typeof column.format == 'function'
                  //   ? column.format(fieldState.value, this.props.rowData)
                  //   : fieldState.value
                  value
                }
                disabled={formEditable && fieldState.disabled}
                InputLabelProps={{
                  shrink: true
                }}
                variant={formEditable ? 'standard' : 'outlined'}
              />
            </Grid>
          );
        }
      }

      return '';
    });
  };

  handleChildTabChange = (event, selectedIndex) => {
    if (this.state.childTabIndex !== selectedIndex) {
      this.setState({
        childTabIndex: selectedIndex
      });
    }
  };

  renderChilds = () => {
    const { classes } = this.props;

    if (this.state.editMode && this.state.sortedChilds.length > 0) {
      return (
        <Paper square elevation={4} className={classes.childTabs}>
          <Tabs
            value={this.state.childTabIndex}
            indicatorColor="primary"
            textColor="primary"
            onChange={this.handleChildTabChange}
            aria-label="childs tab"
          >
            {this.state.sortedChilds.map((child, index) => (
              <Tab key={index} label={child.title} disabled={child.disabled} />
            ))}
          </Tabs>
          {React.cloneElement(
            this.state.sortedChilds[this.state.childTabIndex].child,
            {
              nested: true,
              masterRowData: this.props.rowData
            }
          )}
        </Paper>
      );
    }
  };

  validateForm = () => {
    // let formData = this.populdateFormData();
    var formFieldsState = Object.assign({}, this.state.formFields);
    var formDataState = Object.assign({}, this.state.formData);

    Logger.console.debug('Validate Form');
    Logger.console.debug('Fields State: ', formFieldsState);
    Logger.console.debug('Form Data: ', formDataState);

    let success = true;
    let errorField = false;
    this.state.sortedColumns.map((column, i) => {
      if (column.editable == 'none') {
        // skip validate column
        return false;
      }

      let field = formFieldsState[column.field];
      // Logger.console.debug(column.field + ' : ' + field.value);
      // support check required function
      let required = false;
      if (typeof column.required === 'function') {
        required = column.required(formDataState);
      } else {
        required = column.required;
      }
      // let fieldValue = formDataState[column.field];
      if (
        required &&
        (field.value == undefined || field.value == null || field.value == '')
      ) {
        Logger.console.debug('Error Field: ' + column.field);
        field.error = true;
        field.message = i18n.t('error.requiredField');
        success = false;

        if (!errorField) {
          errorField = field;
          Logger.console.debug('Set Error Field: ' + column.field);
        }
      } else if (field.value !== '' && column.validate !== undefined) {
        let valid = this.validateField(column, field.value, formDataState);
        field.error = valid.error;
        field.message = valid.message;
        if (field.error && !errorField) {
          success = false;
          errorField = field;
          Logger.console.debug('Set Error Field: ' + column.field);
        }
      } else {
        field.error = false;
        field.message = '';
      }
    });

    this.setState({
      formFields: formFieldsState
    });

    if (errorField && errorField.ref && errorField.ref.focus) {
      errorField.ref.focus();
    }

    return success;
  };

  validateField = (column, value, formData) => {
    let valid = {
      error: false,
      message: ''
    };
    if (column.validate !== undefined) {
      Logger.console.debug(
        'On Field Validation: [' + column.field + ' = ' + value + ']'
      );

      if (typeof column.validate == 'function') {
        valid = column.validate(value, formData);
      } else {
        if (this.validator[column.validate]) {
          valid = this.validator[column.validate](value, formData);
        } else {
          Logger.console.debug('Validator not found: ', column.validate);
        }
      }
    }
    return valid;
  };

  handleSubmit = event => {
    event.preventDefault();

    if (this.validateForm()) {
      // call add/edit action after validate success

      // reduce form data fields to only editable field

      let formDataState = {};
      this.state.sortedColumns.map(column => {
        if (
          column.editable === 'always' ||
          (column.editable === 'onAdd' && !this.state.editMode) ||
          column.primaryKey
        ) {
          // formDataState[column.field] = this.state.formData[column.field];
          _.set(
            formDataState,
            column.field,
            _.get(this.state.formData, column.field, '')
          );
        }
      });
      Logger.console.debug('Editable Form Data: ', formDataState);

      let result = this.state.editMode
        ? this.props.onEdit(formDataState)
        : this.props.onAdd(formDataState);
      result
        .then(res => {
          this.props.onClose();
          this.state.editMode
            ? this.props.onEditSuccess(res)
            : this.props.onAddSuccess(res);
        })
        .catch(error => {});
    }
  };
}

DataFormDialog.propTypes = {
  open: PropTypes.bool,
  title: PropTypes.string,
  columns: PropTypes.arrayOf(
    PropTypes.shape({
      title: PropTypes.string.isRequired,
      field: PropTypes.string.isRequired,
      editable: PropTypes.oneOf(['never', 'always', 'onAdd', 'none']),
      required: PropTypes.oneOfType([PropTypes.bool, PropTypes.func]),
      primaryKey: PropTypes.bool,
      render: PropTypes.func,
      format: PropTypes.func,
      colspan: PropTypes.number,
      disabled: PropTypes.oneOfType([PropTypes.bool, PropTypes.func]),
      validate: PropTypes.oneOfType([
        PropTypes.oneOf(['userId', 'password', 'email', 'date']),
        PropTypes.func
      ]),
      order: PropTypes.number,
      // support lookup value on data form
      lookup: PropTypes.func
    })
  ),
  rowData: PropTypes.object,
  onClose: PropTypes.func,
  onAdd: PropTypes.func,
  onEdit: PropTypes.func,
  onAddSuccess: PropTypes.func,
  onEditSuccess: PropTypes.func,
  editable: PropTypes.bool,
  childs: PropTypes.arrayOf(
    PropTypes.shape({
      order: PropTypes.number,
      title: PropTypes.string,
      disabled: PropTypes.oneOfType([PropTypes.bool, PropTypes.func]),
      child: PropTypes.node
    })
  ),
  nested: PropTypes.bool,
  columnsPerRow: PropTypes.oneOf([12, 6, 4, 3, 2, 1])
};

DataFormDialog.defaultProps = {
  editable: true
};

const mapStateToProps = state => {
  return {
    login: state.login,
    lang: state.i18n.locale
  };
};

const mapDispatchToProps = dispatch => ({});

const dataFormDialog = connect(
  mapStateToProps,
  mapDispatchToProps
)(DataFormDialog);

export default withStyles(styles, { withTheme: true })(dataFormDialog);
