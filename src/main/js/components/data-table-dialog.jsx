import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle
} from '@material-ui/core';
import classNames from 'classnames';
import PropTypes from 'prop-types';
import React, { Component } from 'react';

import MuiDataTable from './mui-data-table.jsx';

const i18n = require('react-redux-i18n').I18n;

class DataTableDialog extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  render() {
    return (
      <Dialog
        disableBackdropClick
        disableEscapeKeyDown
        maxWidth="xs"
        fullWidth={true}
        scroll={'paper'}
        open={this.props.open}
        onClose={this.props.onClose}
        aria-labelledby="data-table-dialog-title"
        aria-describedby="data-table-dialog-description"
      >
        <DialogContent>
          {React.cloneElement(this.props.children, {
            multiSelect: this.props.multiSelect
          })}
        </DialogContent>
        <DialogActions>
          {this.props.multiSelect ? (
            <Button onClick={this.props.onConfirm} color="primary">
              {i18n.t('actions.confirm')}
            </Button>
          ) : (
            ''
          )}
          <Button onClick={this.props.onClose} autoFocus>
            {i18n.t('actions.cancel')}
          </Button>
        </DialogActions>
      </Dialog>
    );
  }
}

DataTableDialog.propTypes = {
  open: PropTypes.bool.isRequired,
  multiSelect: PropTypes.bool,
  onClose: PropTypes.func,
  onConfirm: PropTypes.func
};

DataTableDialog.defaultProps = {
  open: false,
  multiSelect: false,
  onClose: () => {},
  onConfirm: () => {}
};

export default DataTableDialog;
