import {
  IconButton,
  Slide,
  Snackbar,
  SnackbarContent,
  withStyles
} from '@material-ui/core';
import { amber, green } from '@material-ui/core/colors';
import {
  CheckCircle as CheckCircleIcon,
  Close as CloseIcon,
  Error as ErrorIcon,
  Info as InfoIcon,
  Warning as WarningIcon
} from '@material-ui/icons';
import classNames from 'classnames';
import PropTypes from 'prop-types';
import React, { Component } from 'react';

// import { makeStyles } from '@material-ui/styles';

const variantIcon = {
  success: <CheckCircleIcon />,
  warning: <WarningIcon />,
  error: <ErrorIcon />,
  info: <InfoIcon />
};

const styles = theme => ({
  root: {
    maxWidth: 600
  },
  success: {
    backgroundColor: green[600]
  },
  error: {
    backgroundColor: theme.palette.error.dark
  },
  info: {
    backgroundColor: theme.palette.primary.main
  },
  warning: {
    backgroundColor: amber[700]
  },
  icon: {
    fontSize: 20
  },
  iconVariant: {
    opacity: 0.9,
    marginRight: theme.spacing(1)
  },
  message: {
    display: 'flex',
    alignItems: 'center'
  }
});

const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="down" ref={ref} {...props} />;
});

class MessageContent extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    const {
      classes,
      className,
      message,
      onClose,
      variant,
      ...other
    } = this.props;
    const icon = variantIcon[variant];
    return (
      <SnackbarContent
        className={classNames(classes[variant], className)}
        aria-describedby="client-snackbar"
        message={
          <span id="client-snackbar" className={classes.message}>
            <span className={classNames(classes.icon, classes.iconVariant)}>
              {icon}
            </span>
            {message}
          </span>
        }
        action={[
          <IconButton
            key="close"
            aria-label="close"
            color="inherit"
            onClick={onClose}
          >
            <CloseIcon className={classes.icon} />
          </IconButton>
        ]}
        {...other}
      />
    );
  }
}

MessageContent.propTypes = {
  className: PropTypes.string,
  message: PropTypes.string,
  onClose: PropTypes.func,
  variant: PropTypes.oneOf(['error', 'info', 'success', 'warning'])
};

MessageContent.defaultProps = {
  variant: 'info'
};

class Message extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  render() {
    const { classes } = this.props;
    return (
      <Snackbar
        anchorOrigin={{
          vertical: 'top',
          horizontal: 'center'
        }}
        open={this.props.open}
        // autoHideDuration={5000}
        onClose={this.props.onClose}
        className={classes.root}
        TransitionComponent={Transition}
      >
        <MessageContent
          onClose={this.props.onClose}
          variant={this.props.variant}
          message={this.props.content}
          classes={this.props.classes}
        />
      </Snackbar>
    );
  }
}

Message.propTypes = {
  open: PropTypes.bool,
  variant: PropTypes.oneOf(['error', 'info', 'success', 'warning']),
  content: PropTypes.string,
  onClose: PropTypes.func
};

MessageContent.defaultProps = {
  open: false,
  variant: 'info'
};

// export default Message;
export default withStyles(styles, { withTheme: true })(Message);
