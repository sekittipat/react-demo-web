import { withStyles } from '@material-ui/core';
import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { connect } from 'react-redux';

import Logger from '../utils/logger.jsx';
import DataFormDialog from './data-form-dialog.jsx';
import MuiDataTable from './mui-data-table.jsx';

const i18n = require('react-redux-i18n').I18n;

const styles = theme => ({
  appBar: {
    position: 'relative'
  },
  title: {
    marginLeft: theme.spacing(2),
    flex: 1
  }
});

class DataTable extends Component {
  constructor(props) {
    super(props);

    this.state = {
      dataFormDialogOpen: false,
      rowData: undefined
    };

    this.tableRef = React.createRef();
  }

  render() {
    // initial table columns
    let tableColumns = [];

    this.props.columns.map((column, index) => {
      if (column.viewable) {
        tableColumns.push({
          title: column.title,
          field: column.field,
          emptyValue: '-',
          ...column.viewOptions
        });
      }
    });

    return (
      <React.Fragment>
        <MuiDataTable
          columns={tableColumns}
          data={query => this.onSearch(query)}
          title={this.props.title}
          onAdd={this.onAdd}
          onEdit={this.onEdit}
          onDelete={this.onDelete}
          rowActions={this.props.rowActions}
          tableActions={this.props.tableActions}
          searchInterval={600}
          ref={this.tableRef}
          editable={this.props.editable}
          onView={this.onView}
          // multiSelect={this.props.multiSelect}
          selectionMode={this.props.selectionMode}
          // enableSelectAll={this.props.enableSelectAll}
          onSelect={this.props.onSelect}
        />
        {this.renderDataFormDialog()}
      </React.Fragment>
    );
  }

  onSearch = query => {
    let sorts = [];
    if (query.orderBy) {
      sorts.push({
        fieldName: query.orderBy,
        sortType: query.orderDirection
      });
    }
    let filters = [];
    if (query.filters && query.filters.length > 0) {
      query.filters.map((v, i) => {
        filters.push({
          fieldName: v.column.field,
          values: Array.isArray(v.value) ? v.value : [v.value]
        });
      });
    }
    return this.props.onSearch(query, sorts, filters);
  };

  onAdd = event => {
    Logger.console.debug('Add Data: ');
    this.setState({
      dataFormDialogOpen: true,
      // addMode: true,
      rowData: undefined
    });
  };

  onEdit = (event, rowData) => {
    Logger.console.debug('Edit Data: ', rowData);
    this.setState({
      dataFormDialogOpen: true,
      // addMode: false,
      rowData: rowData
    });
  };

  onDelete = (event, rowData) => {
    Logger.console.debug('Delete Data: ', rowData);
    this.props
      .onDelete(rowData)
      .then(res => {
        this.refreshDataTable();
      })
      .catch(error => {});
  };

  onView = (event, rowData) => {
    Logger.console.debug('View Data: ', rowData);
    this.setState({
      dataFormDialogOpen: true,
      // addMode: false,
      rowData: rowData
    });
  };

  onDataFormDialogClose = () => {
    this.setState({
      dataFormDialogOpen: false
    });
  };

  renderDataFormDialog = () => {
    if (this.state.dataFormDialogOpen) {
      let formColumns = [];

      this.props.columns.map((column, index) => {
        if (column.editable) {
          formColumns.push({
            title: column.title,
            field: column.field,
            // required: column.required,
            editable: column.editable,
            // render: column.render
            // emptyValue: '-',
            ...column.editOptions
          });
        }
      });
      return (
        <DataFormDialog
          open={true}
          title={this.props.title}
          columns={formColumns}
          rowData={this.state.rowData}
          onClose={this.onDataFormDialogClose}
          onAdd={this.props.onAdd}
          onEdit={this.props.onEdit}
          onAddSuccess={this.refreshDataTable}
          onEditSuccess={this.refreshCurrentPage}
          editable={this.props.editable}
          childs={this.props.childs}
          nested={this.props.nested}
          columnsPerRow={this.props.formColumns}
        />
      );
    }

    return '';
  };

  refreshDataTable = () => {
    this.tableRef.current.refreshData(true, true);
  };

  refreshCurrentPage = () => {
    this.tableRef.current.refreshData(false, false);
  };

  getSelectedRows = () => {
    return this.tableRef.current.getSelectedRows();
  };
}

DataTable.propTypes = {
  title: PropTypes.string,
  columns: PropTypes.arrayOf(
    PropTypes.shape({
      title: PropTypes.string.isRequired,
      field: PropTypes.string.isRequired,
      viewable: PropTypes.bool,
      viewOptions: PropTypes.object, // material-table options
      editable: PropTypes.oneOf(['never', 'always', 'onAdd', 'none']),
      editOptions: PropTypes.shape({
        required: PropTypes.oneOfType([PropTypes.bool, PropTypes.func]),
        primaryKey: PropTypes.bool,
        render: PropTypes.func,
        format: PropTypes.func,
        colspan: PropTypes.number,
        disabled: PropTypes.oneOfType([PropTypes.bool, PropTypes.func]),
        validate: PropTypes.oneOfType([
          PropTypes.oneOf(['userId', 'password', 'email', 'date']),
          PropTypes.func
        ]),
        order: PropTypes.number,
        // support lookup value on data form
        lookup: PropTypes.oneOfType([PropTypes.array, PropTypes.func])
      })
    })
  ),
  onSearch: PropTypes.func,
  onAdd: PropTypes.func,
  onEdit: PropTypes.func,
  onDelete: PropTypes.func,
  actions: PropTypes.array,
  editable: PropTypes.bool,
  childs: PropTypes.arrayOf(
    PropTypes.shape({
      order: PropTypes.number,
      title: PropTypes.string,
      disabled: PropTypes.oneOfType([PropTypes.bool, PropTypes.func]),
      child: PropTypes.node
    })
  ),
  nested: PropTypes.bool,
  formColumns: PropTypes.oneOf([12, 6, 4, 3, 2, 1]),
  // multiSelect: PropTypes.bool,
  selectionMode: PropTypes.oneOf(['single', 'multiple']),
  // enableSelectAll: PropTypes.bool,
  onSelect: PropTypes.func
};

DataTable.defaultProps = {
  editable: true
};

const mapStateToProps = state => {
  return {
    login: state.login,
    lang: state.i18n.locale
  };
};

const mapDispatchToProps = dispatch => ({});

const DataTableRedux = connect(mapStateToProps, mapDispatchToProps, null, {
  forwardRef: true
})(DataTable);

const DataTableReduxWithStyles = withStyles(styles, { withTheme: true })(
  DataTableRedux
);

export default React.forwardRef((props, ref) => (
  <DataTableReduxWithStyles {...props} ref={ref} />
));
