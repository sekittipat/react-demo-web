import AppBar from '@material-ui/core/AppBar';
import Box from '@material-ui/core/Box';
import Divider from '@material-ui/core/Divider';
import IconButton from '@material-ui/core/IconButton';
import ListSubheader from '@material-ui/core/ListSubheader';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import { withStyles } from '@material-ui/core/styles';
import Toolbar from '@material-ui/core/Toolbar';
import Tooltip from '@material-ui/core/Tooltip';
import Typography from '@material-ui/core/Typography';
import AccountCircle from '@material-ui/icons/AccountCircle';
import MenuIcon from '@material-ui/icons/Menu';
import React from 'react';
import { connect } from 'react-redux';
import { setLocale } from 'react-redux-i18n';
import { Link } from 'react-router-dom';

import { sessionManager } from '../utils/session.jsx';
import SideBar from './side-bar.jsx';

const i18n = require('react-redux-i18n').I18n;

const moment = require('moment');

const styles = theme => ({
  drawerHeader: {
    display: 'flex',
    alignItems: 'center',
    padding: '0 8px',
    ...theme.mixins.toolbar,
    justifyContent: 'flex-start'
  },
  subMenuItem: {
    paddingLeft: theme.spacing(4)
  },
  appName: {
    lineHeight: 'unset',
    [theme.breakpoints.down('xs')]: {
      fontSize: theme.typography.subtitle1.fontSize
    }
  },
  appVersion: {
    color: '#ccc'
  },
  textCaption: {
    color: '#ccc'
  },
  itemIcon: {
    minWidth: 'unset',
    margin: theme.spacing(0, 1, 0, 0)
  },
  userBox: {
    [theme.breakpoints.down('xs')]: {
      display: 'none'
    }
  }
});

class TopBar extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      menuOpen: {
        accountMenu: false,
        languageMenu: false,
        sideMenu: false
        // masterData: false,
        // crm: false
      }
    };
  }

  render() {
    return (
      <React.Fragment>
        {this.renderAppToolbar()}
        {this.renderSideMenu()}
      </React.Fragment>
    );
  }

  renderAppToolbar = () => {
    const { classes } = this.props;
    return (
      <AppBar>
        <Toolbar>
          {this.renderSideMenuToggle()}
          <Box
            component="span"
            display="flex"
            flexDirection="column"
            flexGrow={1}
          >
            <Typography
              variant="h6"
              display="block"
              className={classes.appName}
            >
              {/* {window.appContext.name} */}
              {this.renderAppName()}
            </Typography>

            <Typography
              variant="caption"
              display="block"
              noWrap
              className={classes.appVersion}
            >
              {window.appContext.version}
            </Typography>
          </Box>
          {this.renderUserAccountMenu()}
          {this.renderLanguageMenu()}
        </Toolbar>
      </AppBar>
    );
  };

  renderAppName = () => {
    let appName = window.appContext.name;
    let appEnv = window.appContext.env;
    if (appEnv && appEnv !== 'prd') {
      appName = `${appName} (${appEnv.toUpperCase()})`;
    }

    return appName;
  };

  openSideMenu = () => {
    this.openMenu('sideMenu');
  };

  closeSideMenu = () => {
    this.closeMenu('sideMenu');
  };

  renderSideMenuToggle = () => {
    if (this.props.login.isLoggedIn) {
      return (
        <Tooltip title={i18n.t('tooltip.openMenu')}>
          <IconButton
            edge="start"
            color="inherit"
            aria-label="Menu"
            onClick={this.openSideMenu}
          >
            <MenuIcon />
          </IconButton>
        </Tooltip>
      );
    }

    return '';
  };

  renderSideMenu = () => {
    return (
      <SideBar
        open={this.state.menuOpen['sideMenu']}
        onClose={this.closeSideMenu}
      />
    );
  };

  openUserAccountMenu = event => {
    this.menuRef = event.currentTarget;
    this.openMenu('accountMenu');
  };

  closeUserAccountMenu = () => {
    this.menuRef = null;
    this.closeMenu('accountMenu');
  };

  renderUserAccountMenu = () => {
    const { classes } = this.props;
    if (this.props.login.isLoggedIn) {
      return (
        <React.Fragment>
          <Box
            component="span"
            display="flex"
            flexDirection="column"
            flexGrow={1}
            className={classes.userBox}
          >
            <Typography
              variant="subtitle1"
              display="block"
              align="right"
              noWrap
            >
              {this.props.login.user.username}
            </Typography>
            <Typography
              variant="caption"
              display="block"
              align="right"
              className={classes.textCaption}
              noWrap
            >
              {this.props.login.user.storeCode}{' '}
              {this.props.login.user.storeName}
            </Typography>
          </Box>
          <IconButton
            aria-label="Account of current user"
            aria-controls="menu-appbar"
            aria-haspopup="true"
            onClick={this.openUserAccountMenu}
            color="inherit"
          >
            <AccountCircle />
          </IconButton>
          <Menu
            id="menu-appbar"
            anchorEl={this.menuRef}
            open={this.state.menuOpen['accountMenu']}
            onClose={this.closeUserAccountMenu}
            getContentAnchorEl={null}
            anchorOrigin={{
              vertical: 'bottom',
              horizontal: 'center'
            }}
            transformOrigin={{
              vertical: 'top',
              horizontal: 'center'
            }}
          >
            {/* <MenuItem
              component={Link}
              to="/page/main"
              onClick={this.closeUserAccountMenu}
            >
              Profile
            </MenuItem> */}
            <ListSubheader>
              <Box display="flex" justifyContent="center">
                <AccountCircle />
              </Box>
              <Typography variant="subtitle1" align="center" display="block">
                <strong>{this.props.login.user.username}</strong>
              </Typography>
              <Typography variant="caption" align="center" display="block">
                {this.props.login.user.storeCode}{' '}
                {this.props.login.user.storeName}
              </Typography>
            </ListSubheader>
            <Divider />
            <MenuItem
              component={Link}
              to="/page/logout"
              onClick={this.closeUserAccountMenu}
            >
              {i18n.t('menu.logout')}
            </MenuItem>
          </Menu>
        </React.Fragment>
      );
    }

    return '';
  };

  openLanguageMenu = event => {
    this.menuLanguageRef = event.currentTarget;
    this.openMenu('languageMenu');
  };

  closeLanguageMenu = () => {
    this.menuLanguageRef = null;
    this.closeMenu('languageMenu');
  };

  toggleLanguage = locale => {
    sessionManager.setLocale(locale);
    moment.locale(locale); // set global moment locale

    this.closeLanguageMenu();

    this.props.changeLocale(locale);
    // window.location.reload();
  };

  renderLanguageMenu = () => {
    let imgName =
      this.props.lang === 'th' ? 'flag_th_flat_24.png' : 'flag_en_flat_24.png';
    let imgSrc = window.appContext.contextPath + '/images/' + imgName;

    return (
      <React.Fragment>
        <Tooltip title={i18n.t('tooltip.changeLanguage')}>
          <IconButton
            aria-label="Change language"
            aria-controls="menu-language"
            aria-haspopup="true"
            onClick={this.openLanguageMenu}
            color="inherit"
          >
            <img src={imgSrc} />
          </IconButton>
        </Tooltip>
        <Menu
          id="menu-language"
          anchorEl={this.menuLanguageRef}
          open={this.state.menuOpen['languageMenu']}
          onClose={this.closeLanguageMenu}
          getContentAnchorEl={null}
          anchorOrigin={{
            vertical: 'bottom',
            horizontal: 'center'
          }}
          transformOrigin={{
            vertical: 'top',
            horizontal: 'center'
          }}
        >
          <MenuItem
            // component={Link}
            // to="/page/main"
            onClick={() => this.toggleLanguage('th')}
          >
            ไทย
          </MenuItem>
          <MenuItem
            // component={Link}
            // to="/page/logout"
            onClick={() => this.toggleLanguage('en')}
          >
            English
          </MenuItem>
        </Menu>
      </React.Fragment>
    );
  };

  openMenu = key => {
    let newMenuOpen = Object.assign({}, this.state.menuOpen);
    newMenuOpen[key] = true;
    this.setState({
      menuOpen: newMenuOpen
    });
  };

  closeMenu = key => {
    let newMenuOpen = Object.assign({}, this.state.menuOpen);
    newMenuOpen[key] = false;
    this.setState({
      menuOpen: newMenuOpen
    });
  };
}

const mapStateToProps = state => {
  return {
    login: state.login,
    lang: state.i18n.locale
  };
};

const mapDispatchToProps = dispatch => ({
  // logoutSuccess: () => dispatch(logoutSuccess())
  changeLocale: locale => dispatch(setLocale(locale))
});

const topBar = connect(mapStateToProps, mapDispatchToProps)(TopBar);

export default withStyles(styles)(topBar);
