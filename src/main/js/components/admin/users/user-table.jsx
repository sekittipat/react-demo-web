import {
  Checkbox,
  FormControl,
  FormControlLabel,
  FormHelperText,
  IconButton,
  InputAdornment,
  InputLabel,
  MenuItem,
  Select,
  TextField
} from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';
import { Visibility, VisibilityOff } from '@material-ui/icons';
import axios from 'axios';
import React, { Component } from 'react';
import { connect } from 'react-redux';

import { DataRestAPI } from '../../../utils/constants.jsx';
import Logger from '../../../utils/logger.jsx';
import { renderAPIErrorMessage } from '../../../utils/messages.jsx';
import DataTable from '../../data-table.jsx';
import MessageDialog from '../../message-dialog.jsx';
import Message from '../../message.jsx';

const i18n = require('react-redux-i18n').I18n;

const moment = require('moment');

const styles = (theme) => ({
  table: {
    margin: theme.spacing(3, 2)
  }
});

class UserTable extends Component {
  constructor(props) {
    super(props);

    this.state = {
      message: {
        open: false
      },
      messageDialog: {
        open: false,
        confirm: false,
        variant: 'info',
        title: '',
        content: '',
        onConfirm: () => {},
        onCancel: () => {}
      },
      showPassword: false
    };
  }

  render() {
    // const { classes, theme } = this.props;
    // Logger.console.debug('Style: ', classes, theme);
    let formatDateTime = (value) => {
      if (value !== '-' && moment(value).isValid()) {
        return moment(value).format('DD/MM/YYYY HH:mm:ss');
      }
      return value;
    };

    let tableColumns = [
      {
        title: i18n.t('pages.admin.user.userTable.userId'),
        field: 'userId',
        viewable: true,
        viewOptions: {
          // editable: 'onAdd',
          defaultSort: 'asc',
          filtering: false,
          searchable: true
        },
        editable: 'onAdd',
        editOptions: {
          required: true,
          primaryKey: true,
          validate: 'userId',
          render: (props, index) => (
            <TextField
              key={index}
              margin="normal"
              label={i18n.t('pages.admin.user.userTable.userId')}
              fullWidth
              value={props.value}
              onChange={(e) => props.onChange(e.target.value)}
              onKeyDown={(e) => props.onKeyDown(e)}
              error={props.error}
              helperText={props.message}
              disabled={props.disabled}
              // autoFocus={!props.editmode}
              // variant="outlined"
              inputRef={(el) => (props.ref = el)}
              inputProps={{
                maxLength: 50
              }}
              InputLabelProps={{
                shrink: true
              }}
            />
          )
        }
      },
      {
        title: i18n.t('pages.admin.user.userTable.userPwd'),
        field: 'userPwd',
        // viewable: false,
        // viewOptions: {
        //   filtering: false
        // },
        editable: 'onAdd',
        editOptions: {
          required: (formData) => formData.userRole < 90,
          validate: 'password',
          render: (props, index, formData) => (
            <TextField
              key={index}
              margin="normal"
              label={i18n.t('pages.admin.user.userTable.userPwd')}
              fullWidth
              type={this.state.showPassword ? 'text' : 'password'}
              value={props.value}
              onChange={(e) => props.onChange(e.target.value)}
              error={props.error}
              helperText={props.message}
              // disabled={props.disabled}
              disabled={formData.userRole >= 90}
              autoFocus={props.editMode}
              // variant="outlined"
              inputRef={(el) => (props.ref = el)}
              inputProps={{
                maxLength: 100
              }}
              InputProps={{
                endAdornment: (
                  <InputAdornment position="end">
                    <IconButton
                      aria-label="toggle password visibility"
                      onClick={this.handleClickShowPassword}
                      onMouseDown={this.handleMouseDownPassword}
                    >
                      {this.state.showPassword ? (
                        <Visibility />
                      ) : (
                        <VisibilityOff />
                      )}
                    </IconButton>
                  </InputAdornment>
                )
              }}
              InputLabelProps={{
                shrink: true
              }}
            />
          )
        }
      },
      {
        title: i18n.t('pages.admin.user.userTable.email'),
        field: 'email',
        viewable: true,
        viewOptions: {
          filtering: true
        },
        editable: 'always',
        editOptions: {
          required: false,
          validate: 'email',
          render: (props, index) => (
            <TextField
              key={index}
              margin="normal"
              label={i18n.t('pages.admin.user.userTable.email')}
              fullWidth
              value={props.value}
              onChange={(e) => props.onChange(e.target.value)}
              error={props.error}
              helperText={props.message}
              disabled={props.disabled}
              // variant="outlined"
              inputRef={(el) => (props.ref = el)}
              inputProps={{
                maxLength: 100
              }}
              InputLabelProps={{
                shrink: true
              }}
            />
          )
        }
      },
      {
        title: i18n.t('pages.admin.user.userTable.accountType'),
        field: 'accountType',
        viewable: true,
        viewOptions: {
          // lookup: { 1: 'LocalDB', 2: 'Domain' }
          render: (rowData) => {
            return `${rowData.accountType}:${rowData.accountTypeDesc}`;
          }
        },
        editable: 'onAdd',
        editOptions: {
          order: 2,
          required: true,
          render: (props, index, formData) => (
            <FormControl
              error={props.error}
              margin="normal"
              fullWidth
              key={index}
            >
              <InputLabel htmlFor="accountType" shrink>
                {i18n.t('pages.admin.user.userTable.accountType')}
              </InputLabel>
              <Select
                id="accountType"
                value={props.value}
                onChange={(e) => {
                  // clear domainName field if account type is LocalDB
                  let otherFields = [];
                  if (e.target.value === 1) {
                    otherFields = [
                      {
                        name: 'domainName',
                        value: ''
                      }
                    ];
                  }
                  // clear requireChangePwd field if account type is Domain
                  if (e.target.value === 2) {
                    otherFields = [
                      {
                        name: 'requireChangePwd',
                        value: 'N'
                      }
                    ];
                  }
                  props.onChange(e.target.value, otherFields);
                }}
                // onBlur={props.validateForm}
                inputRef={(el) => (props.ref = el)}
                disabled={
                  props.disabled ||
                  formData.userRole >= 90 ||
                  !props.lookup.ready
                }
                displayEmpty
              >
                <MenuItem value="">{`--- ${
                  props.lookup.ready ? 'Select' : 'Loading'
                } ---`}</MenuItem>
                {props.lookup.items}
              </Select>
              <FormHelperText key={index}>{props.message}</FormHelperText>
            </FormControl>
          ),
          lookup: (formData) => {
            Logger.console.debug(`Lookup Account Type: Form Data: `, formData);

            return [
              <MenuItem key={1} value={1}>
                LocalDB
              </MenuItem>,
              <MenuItem key={2} value={2}>
                Domain
              </MenuItem>
            ];
          }
        }
      },
      {
        title: i18n.t('pages.admin.user.userTable.domainName'),
        field: 'domainName',
        viewable: true,
        viewOptions: {
          filtering: false
        },
        editable: 'onAdd',
        editOptions: {
          order: 3,
          required: (formData) => formData.accountType == 2,
          render: (props, index, formData) => (
            <FormControl
              error={props.error}
              margin="normal"
              fullWidth
              key={index}
            >
              <InputLabel htmlFor="domainName" shrink>
                {i18n.t('pages.admin.user.userTable.domainName')}
              </InputLabel>
              <Select
                id="domainName"
                value={props.value}
                onChange={(e) => props.onChange(e.target.value)}
                // onBlur={props.validateForm}
                inputRef={(el) => (props.ref = el)}
                disabled={
                  props.disabled ||
                  formData.accountType !== 2 ||
                  !props.lookup.ready
                }
                displayEmpty
              >
                <MenuItem value="">{`--- ${
                  props.lookup.ready ? 'Select' : 'Loading'
                } ---`}</MenuItem>
                {props.lookup.items}
              </Select>
              <FormHelperText key={index}>{props.message}</FormHelperText>
            </FormControl>
          ),
          lookup: (formData) => {
            Logger.console.debug(`Lookup Domain Name: Form Data: `, formData);

            return [];
          }
        }
      },
      {
        title: i18n.t('pages.admin.user.userTable.requireChangePwd'),
        field: 'requireChangePwd',
        viewable: true,
        viewOptions: {
          lookup: { Y: 'Yes', N: 'No' },
          filtering: false
        },
        editable: 'always',
        editOptions: {
          required: (formData) =>
            formData.accountType == 1 && formData.userRole < 90, // required if accountType is LocalDB
          defaultValue: 'N',
          render: (props, index, formData) => (
            // <FormControl
            //   error={props.error}
            //   margin="normal"
            //   fullWidth
            //   key={index}
            // >
            //   <InputLabel htmlFor="requireChangePwd">
            //     {i18n.t('pages.admin.user.userTable.requireChangePwd')}
            //   </InputLabel>
            //   <Select
            //     id="requireChangePwd"
            //     value={props.value}
            //     onChange={e => props.onChange(e.target.value)}
            //     // onBlur={props.validateForm}
            //     inputRef={el => (props.ref = el)}
            //     disabled={formData.accountType !== 1 || formData.userRole >= 90} // disabled if accountType is not LocaleDB
            //   >
            //     <MenuItem value={'Y'}>Yes</MenuItem>
            //     <MenuItem value={'N'}>No</MenuItem>
            //   </Select>
            //   <FormHelperText key={index}>{props.message}</FormHelperText>
            // </FormControl>
            <FormControlLabel
              label={i18n.t('pages.admin.user.userTable.requireChangePwd')}
              control={
                <Checkbox
                  key={index}
                  margin="normal"
                  value={props.value}
                  checked={props.value == 'Y'}
                  onChange={(e) => props.onChange(e.target.checked ? 'Y' : 'N')}
                  inputRef={(el) => (props.ref = el)}
                  disabled={
                    formData.accountType !== 1 || formData.userRole >= 90
                  } // disabled if accountType is not LocaleDB
                />
              }
            />
          )
        }
      },
      {
        title: i18n.t('pages.admin.user.userTable.userRole'),
        field: 'userRole',
        viewable: true,
        viewOptions: {
          // lookup: { 1: 'Admin', 2: 'User', 90: 'API' }
          render: (rowData) => {
            return `${rowData.userRole}:${rowData.userRoleDesc}`;
          }
        },
        editable: 'onAdd',
        editOptions: {
          order: 1,
          required: true,
          render: (props, index, formData) => (
            <FormControl
              error={props.error}
              margin="normal"
              fullWidth
              key={index}
            >
              <InputLabel htmlFor="userRole" shrink>
                {i18n.t('pages.admin.user.userTable.userRole')}
              </InputLabel>
              <Select
                id="userRole"
                value={props.value}
                // onChange={e => props.onChange(e.target.value)}
                // onBlur={props.validateForm}
                inputRef={(el) => (props.ref = el)}
                inputProps={{
                  autoFocus: !props.editmode
                }}
                disabled={props.disabled || !props.lookup.ready}
                onChange={(e) => {
                  // set account type to LocalDB if user role is API
                  let otherFields = [
                    {
                      name: 'accountType',
                      value:
                        e.target.value >= 90
                          ? 1
                          : props.editMode
                          ? formData.accountType
                          : ''
                    },
                    {
                      name: 'userPwd',
                      value:
                        e.target.value >= 90
                          ? ''
                          : props.editMode
                          ? formData.userPwd
                          : ''
                    },
                    {
                      name: 'requireChangePwd',
                      value:
                        e.target.value >= 90
                          ? 'N'
                          : props.editMode
                          ? formData.requireChangePwd
                          : 'N'
                    },
                    {
                      name: 'domainName',
                      value:
                        e.target.value >= 90
                          ? ''
                          : props.editMode
                          ? formData.domainName
                          : ''
                    }
                  ];

                  props.onChange(e.target.value, otherFields);
                }}
                displayEmpty
              >
                <MenuItem value="">{`--- ${
                  props.lookup.ready ? 'Select' : 'Loading'
                } ---`}</MenuItem>
                {/* <MenuItem value={1}>Admin</MenuItem>
                <MenuItem value={2}>User</MenuItem>
                <MenuItem value={90}>API</MenuItem> */}
                {props.lookup.items}
              </Select>
              <FormHelperText key={index}>{props.message}</FormHelperText>
            </FormControl>
          ),
          lookup: (loadAll, formData) => {
            Logger.console.debug(`Lookup User Role: Form Data: `, formData);

            return [
              <MenuItem key={1} value={1}>
                Admin
              </MenuItem>,
              <MenuItem key={2} value={2}>
                User
              </MenuItem>,
              <MenuItem key={90} value={90}>
                API
              </MenuItem>
            ];
          }
        }
      },
      {
        title: i18n.t('pages.admin.user.userTable.activeFlag'),
        field: 'activeFlag',
        viewable: true,
        viewOptions: {
          lookup: { Y: 'Yes', N: 'No' }
        },
        editable: 'always',
        editOptions: {
          required: true,
          defaultValue: 'N',
          render: (props, index) => (
            <FormControlLabel
              label={i18n.t('pages.admin.user.userTable.activeFlag')}
              control={
                <Checkbox
                  key={index}
                  margin="normal"
                  value={props.value}
                  checked={props.value == 'Y'}
                  onChange={(e) => props.onChange(e.target.checked ? 'Y' : 'N')}
                  inputRef={(el) => (props.ref = el)}
                />
              }
            />
          )
        }
      },
      {
        title: i18n.t('pages.admin.user.userTable.lastLoginDate'),
        field: 'lastLogin',
        viewable: true,
        viewOptions: {
          filtering: false,
          render: (rowData) => formatDateTime(rowData.lastLogin)
        },
        editable: 'never',
        editOptions: {
          format: (value, rowData) => formatDateTime(value)
        }
      },
      {
        title: i18n.t('pages.admin.user.userTable.lastChangePwd'),
        field: 'lastChangePwd',
        viewable: true,
        viewOptions: {
          filtering: false,
          render: (rowData) => formatDateTime(rowData.lastChangePwd)
        },
        editable: 'never',
        editOptions: {
          format: (value, rowData) => formatDateTime(value)
        }
      },
      {
        title: i18n.t('pages.admin.user.userTable.createDate'),
        field: 'createDate',
        viewable: true,
        viewOptions: {
          filtering: false,
          render: (rowData) => formatDateTime(rowData.createDate)
        },
        editable: 'never',
        editOptions: {
          format: (value, rowData) => formatDateTime(value)
        }
      },
      {
        title: i18n.t('pages.admin.user.userTable.createBy'),
        field: 'createBy',
        viewable: true,
        viewOptions: {
          // editable: 'never',
          filtering: false
        },
        editable: 'never'
      },
      {
        title: i18n.t('pages.admin.user.userTable.lupdDate'),
        field: 'lupdDate',
        viewable: true,
        viewOptions: {
          filtering: false,
          render: (rowData) => formatDateTime(rowData.lupdDate)
        },
        editable: 'never',
        editOptions: {
          format: (value, rowData) => formatDateTime(value)
        }
      },
      {
        title: i18n.t('pages.admin.user.userTable.lupdBy'),
        field: 'lupdBy',
        viewable: true,
        viewOptions: {
          filtering: false
        },
        editable: 'never'
      }
    ];

    return (
      <React.Fragment>
        <DataTable
          title={i18n.t('menu.user')}
          columns={tableColumns}
          // data={query => this.searchUser(query)}
          onSearch={(query, sorts, filters) =>
            this.searchUser(query, sorts, filters)
          }
          onAdd={this.onAddUser}
          onEdit={this.onEditUser}
          onDelete={this.onDeleteUser}
          // multiSelect={true}
          selectionMode="multiple"
        />
        <Message
          open={this.state.message.open}
          variant={this.state.message.type}
          content={this.state.message.content}
          onClose={this.closeMessage}
        />
        <MessageDialog
          confirm={this.state.messageDialog.confirm}
          open={this.state.messageDialog.open}
          title={this.state.messageDialog.title}
          content={this.state.messageDialog.content}
          onClose={this.closeMessageDialog}
          onConfirm={this.state.messageDialog.onConfirm}
          onCancel={this.state.messageDialog.onCancel}
          variant={this.state.messageDialog.variant}
        />
      </React.Fragment>
    );
  }

  handleClickShowPassword = () => {
    this.setState({
      showPassword: !this.state.showPassword
    });
  };

  handleMouseDownPassword = (event) => {
    event.preventDefault();
  };

  searchUser = (query, sorts, filters) => {
    Logger.console.debug('Call Search User Page API: ', query);
    let token = this.props.login.user.token;
    Logger.console.debug('Token: ', token);
    let rootApiUrl = `${DataRestAPI.USER.URL}/search/searchAll`;
    // let sorts = [];
    // if (query.orderBy) {
    //   sorts.push({
    //     fieldName: query.orderBy.field,
    //     sortType: query.orderDirection
    //   });
    // }
    // let filters = [];
    // if (query.filters && query.filters.length > 0) {
    //   query.filters.map((v, i) => {
    //     filters.push({
    //       fieldName: v.column.field,
    //       values: Array.isArray(v.value) ? v.value : [v.value]
    //     });
    //   });
    // }
    let apiParams = {
      keyword: query.search,
      pageNo: query.page,
      pageSize: query.pageSize,
      sorts: sorts,
      filters: filters,
      fields: query.searchColumns
    };
    return new Promise((resolve, reject) => {
      axios
        .post(rootApiUrl, apiParams, {
          headers: {
            Authorization: token
          }
        })
        .then((res) => {
          let result = res.data;
          Logger.console.debug(
            'Get Users Response: ',
            JSON.stringify(result, null, 2)
          );
          resolve({
            data: result.content,
            page: result.number,
            totalCount: result.totalElements
          });
        })
        .catch((error) => {
          if (error.response) {
            Logger.console.debug(
              'Get Users Error: ',
              JSON.stringify(error.response, null, 2)
            );
          }
          // reject(error);
          this.openErrorMessage('Search User Error', error);
          resolve({
            data: [],
            page: 0,
            totalCount: 0
          });
        });
    });
  };

  onAddUser = (rowData) => {
    return this.confirmAddUser(rowData).then((rowData) =>
      this.addUser(rowData)
    );
  };

  confirmAddUser = (rowData) => {
    return new Promise((resolve, reject) => {
      this.openConfirmMessage(
        'Confirm Add User ?',
        'User will be created',
        () => resolve(rowData),
        () => reject(rowData)
      );
    });
  };

  addUser = (rowData) => {
    Logger.console.debug('Add User: ', rowData);

    return new Promise((resolve, reject) => {
      rowData['createBy'] = this.props.login.user.username;
      Logger.console.debug('Call Add User API: ', rowData);
      let token = this.props.login.user.token;
      Logger.console.debug('Token: ', token);

      let addUserUrl = `${DataRestAPI.USER.URL}`;
      axios
        .post(addUserUrl, rowData, {
          headers: {
            Authorization: token
          }
        })
        .then((res) => {
          let result = res.data;
          Logger.console.debug(
            'Add Users Response: ',
            JSON.stringify(result, null, 2)
          );
          this.openMessage('success', 'Add User Success');
          resolve(result);
        })
        .catch((error) => {
          if (error.response) {
            Logger.console.debug(
              'Add Users Error: ',
              JSON.stringify(error.response, null, 2)
            );
          }
          this.openErrorMessage('Add User Error', error);
          reject(error);
        });
    });
  };

  onEditUser = (rowData) => {
    return this.confirmEditUser(rowData).then((rowData) =>
      this.editUser(rowData)
    );
  };

  confirmEditUser = (rowData) => {
    return new Promise((resolve, reject) => {
      this.openConfirmMessage(
        'Confirm Edit User ?',
        'User will be updated',
        () => resolve(rowData),
        () => reject(rowData)
      );
    });
  };

  editUser = (rowData) => {
    Logger.console.debug('Edit User: ', rowData);

    return new Promise((resolve, reject) => {
      rowData['lupdBy'] = this.props.login.user.username;
      Logger.console.debug('Call Edit User API: ', rowData);
      let token = this.props.login.user.token;
      Logger.console.debug('Token: ', token);

      let editUserUrl = `${DataRestAPI.USER.URL}/${rowData.userId}`;
      axios
        .patch(editUserUrl, rowData, {
          headers: {
            Authorization: token
          }
        })
        .then((res) => {
          let result = res.data;
          Logger.console.debug(
            'Update Users Response: ',
            JSON.stringify(result, null, 2)
          );
          this.openMessage('success', 'Update User Success');
          resolve(result);
        })
        .catch((error) => {
          if (error.response) {
            Logger.console.debug(
              'Update Users Error: ',
              JSON.stringify(error.response, null, 2)
            );
          }
          this.openErrorMessage('Update User Error', error);
          reject(error);
        });
    });
  };

  onDeleteUser = (rowData) => {
    return this.confirmDeleteUser(rowData).then((rowData) =>
      this.deleteUser(rowData)
    );
  };

  confirmDeleteUser = (rowData) => {
    return new Promise((resolve, reject) => {
      this.openConfirmMessage(
        'Confirm Delete User ?',
        'User will be deleted',
        () => resolve(rowData),
        () => reject(rowData)
      );
    });
  };

  deleteUser = (rowData) => {
    Logger.console.debug('Delete User: ', rowData);

    return new Promise((resolve, reject) => {
      rowData['lupdBy'] = this.props.login.user.username;
      Logger.console.debug('Call Delete User API: ', rowData);
      let token = this.props.login.user.token;
      Logger.console.debug('Token: ', token);

      let deleteUserUrl = `${DataRestAPI.USER.URL}/${rowData.userId}`;
      axios
        .delete(deleteUserUrl, {
          headers: {
            Authorization: token
          }
        })
        .then((res) => {
          let result = res.data;
          Logger.console.debug(
            'Delete User Response: ',
            JSON.stringify(result, null, 2)
          );
          this.openMessage('success', 'Delete User Success');
          resolve(result);
        })
        .catch((error) => {
          if (error.response) {
            Logger.console.debug(
              'Delete User Error: ',
              JSON.stringify(error.response, null, 2)
            );
          }
          this.openErrorMessage('Delete User Error', error);
          reject(error);
        });
    });
  };

  openMessage = (type, content) => {
    Logger.console.debug('Open message');
    this.setState({
      message: {
        open: true,
        type: type,
        content: content
      }
    });
  };

  closeMessage = () => {
    Logger.console.debug('Close message');
    this.setState({
      message: {
        open: false
      }
    });
  };

  openConfirmMessage = (title, message, onConfirm, onCancel) => {
    Logger.console.debug('Open confirm message');
    this.setState({
      messageDialog: {
        open: true,
        confirm: true,
        variant: 'info',
        title: title,
        content: message,
        onConfirm: onConfirm,
        onCancel: onCancel
      }
    });
  };

  openErrorMessage = (title, error) => {
    Logger.console.debug('Open error message');
    this.setState({
      messageDialog: {
        open: true,
        confirm: false,
        variant: 'error',
        title: title,
        content: renderAPIErrorMessage(error),
        onConfirm: () => {},
        onCancel: () => {}
      }
    });
  };

  closeMessageDialog = () => {
    Logger.console.debug('Close dialog message');
    this.setState({
      messageDialog: {
        open: false,
        confirm: false,
        variant: 'info',
        title: '',
        content: '',
        onConfirm: () => {},
        onCancel: () => {}
      }
    });
  };
}

const mapStateToProps = (state) => {
  return {
    login: state.login,
    lang: state.i18n.locale
  };
};

const mapDispatchToProps = (dispatch) => ({});

const userTable = connect(mapStateToProps, mapDispatchToProps)(UserTable);

export default withStyles(styles, { withTheme: true })(userTable);
