import PropTypes from 'prop-types';

import { sessionManager } from '../utils/session.jsx';

const _ = require('underscore');

const Permission = props => {
  if (sessionManager.isUserLoggedIn()) {
    let userLogin = sessionManager.getLoginUser();
    if (_.intersection(userLogin.roles, props.roles).length > 0) {
      return props.children;
    }
  }

  return '';
};

Permission.propTypes = {
  roles: PropTypes.array.isRequired
};

export default Permission;
