import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  withStyles
} from '@material-ui/core';
import { amber, green } from '@material-ui/core/colors';
import {
  CheckCircle as CheckCircleIcon,
  Error as ErrorIcon,
  Info as InfoIcon,
  Warning as WarningIcon
} from '@material-ui/icons';
import classNames from 'classnames';
import PropTypes from 'prop-types';
import React, { Component } from 'react';

const i18n = require('react-redux-i18n').I18n;

const variantIcon = {
  success: <CheckCircleIcon />,
  warning: <WarningIcon />,
  error: <ErrorIcon />,
  info: <InfoIcon />
};

const styles = theme => ({
  success: {
    backgroundColor: green[600]
  },
  error: {
    backgroundColor: theme.palette.error.dark
  },
  info: {
    backgroundColor: theme.palette.primary.main
  },
  warning: {
    backgroundColor: amber[700]
  },
  icon: {
    fontSize: 20
  },
  iconVariant: {
    opacity: 0.9,
    marginRight: theme.spacing(1)
  },
  dialogTitle: {
    color: '#fff'
  }
});

class MessageDialog extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const { variant, classes } = this.props;
    const icon = variantIcon[variant];
    return (
      <Dialog
        disableBackdropClick
        disableEscapeKeyDown
        maxWidth="xs"
        fullWidth={true}
        scroll={'paper'}
        open={this.props.open}
        onClose={this.props.onClose}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogTitle
          id="alert-dialog-title"
          className={classNames(classes[variant], classes.dialogTitle)}
        >
          <span className={classNames(classes.icon, classes.iconVariant)}>
            {icon}
          </span>
          {this.props.title}
        </DialogTitle>
        <DialogContent>{this.props.content}</DialogContent>
        <DialogActions>
          {this.props.confirm ? (
            <Button onClick={this.onConfirm} color="primary">
              {i18n.t('actions.confirm')}
            </Button>
          ) : (
            ''
          )}
          <Button onClick={this.onCancel} autoFocus>
            {i18n.t('actions.cancel')}
          </Button>
        </DialogActions>
      </Dialog>
    );
  }

  onConfirm = () => {
    this.props.onConfirm();
    this.props.onClose();
  };

  onCancel = () => {
    this.props.onCancel();
    this.props.onClose();
  };
}

MessageDialog.propTypes = {
  open: PropTypes.bool,
  confirm: PropTypes.bool,
  onClose: PropTypes.func,
  onConfirm: PropTypes.func,
  onCancel: PropTypes.func,
  variant: PropTypes.oneOf(['success', 'warning', 'error', 'info']),
  title: PropTypes.string.isRequired,
  content: PropTypes.node.isRequired
};

MessageDialog.defaultProps = {
  open: false,
  confirm: false,
  onClose: () => {},
  onConfirm: () => {},
  onCancel: () => {},
  variant: 'info'
};

export default withStyles(styles)(MessageDialog);
