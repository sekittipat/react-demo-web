export const sessionKeys = {
  LOGIN_USER: 'LOGIN_USER',
  LOCALE: 'LOCALE'
};

export const sessionManager = {
  setLoginUser: loginUser => {
    sessionStorage.setItem(sessionKeys.LOGIN_USER, JSON.stringify(loginUser));
  },
  getLoginUser: () => {
    if (sessionStorage.getItem(sessionKeys.LOGIN_USER) !== null) {
      return JSON.parse(sessionStorage.getItem(sessionKeys.LOGIN_USER));
    }

    return null;
  },
  clearLoginUser: () => {
    sessionStorage.removeItem(sessionKeys.LOGIN_USER);
  },
  isUserLoggedIn: () => {
    return sessionStorage.getItem(sessionKeys.LOGIN_USER) !== null;
  },
  setLocale: locale => {
    sessionStorage.setItem(sessionKeys.LOCALE, locale);
  },
  getLocale: () => {
    return sessionStorage.getItem(sessionKeys.LOCALE);
  }
};
