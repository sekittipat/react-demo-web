const moment = require('moment');

const currency = require('currency.js');

export const DateFormat = {
  DATE: 'DD/MM/YYYY',
  DATE_TIME: 'DD/MM/YYYY HH:mm:ss',
  format: (value, pattern) => {
    if (value && value !== '-' && moment(value).isValid()) {
      return moment(value).format(pattern);
    }
    return value;
  },
  formatDate: value => {
    return DateFormat.format(value, DateFormat.DATE);
  },
  formatDateTime: value => {
    return DateFormat.format(value, DateFormat.DATE_TIME);
  }
};

export const NumberFormat = {
  formatDecimal: (value, precision) => {
    return currency(value, {
      formatWithSymbol: false,
      separator: ',',
      precision: precision
    }).format();
  },
  formatInteger: value => {
    return NumberFormat.formatDecimal(value, 0);
  }
};
