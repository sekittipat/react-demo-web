export const Roles = {
  ROLE_ADMIN: 'Admin'
};

export const DataRestAPI = {
  USER: {
    URL: window.appContext.apiBaseURL.user
  },
  // DATA_SOURCE: {
  //   URL: window.appContext.apiBaseURL.dataSource
  // },
  DATA_SOURCE_CONFIG: {
    URL: window.appContext.apiBaseURL.dataSourceConfig
  },
  LINE_ALERT_JOB: {
    URL: window.appContext.apiBaseURL.lineAlertJob
  },
  RTPMS_CONDITION: {
    URL: window.appContext.apiBaseURL.rtpmsCondition
  },
  RTPMS_STORE_CONDITION: {
    URL: window.appContext.apiBaseURL.rtpmsStoreCondition
  },
  RTPMS_POINT_GROUP: {
    URL: window.appContext.apiBaseURL.rtpmsPointGroup
  }
};
