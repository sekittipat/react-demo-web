import { Typography } from '@material-ui/core';
import React from 'react';

export const renderAPIErrorMessage = error => {
  // let errorMessage = 'Unknown API Error';
  let status = '',
    errorCode = '',
    errorDesc = '';
  if (error.response) {
    status = error.response.status || 'N/A';
    errorCode =
      (error.response.data &&
        (error.response.data.error || error.response.data.status)) ||
      'N/A';
    errorDesc =
      (error.response.data &&
        (error.response.data.error_description ||
          error.response.data.message ||
          error.response.data.statusMessage)) ||
      'N/A';
  } else {
    status = 'N/A';
    errorCode = 'N/A';
    errorDesc = error.message || 'N/A';
  }

  return (
    <React.Fragment>
      <Typography variant="body1">
        <b>Status:</b> {status}
      </Typography>
      <Typography variant="body1">
        <b>Error Code:</b> {errorCode}
      </Typography>
      <Typography variant="body1">
        <b>Error Description:</b> {errorDesc}
      </Typography>
    </React.Fragment>
  );
};
