const log = require('loglevel');

const isProd = window.appContext.production;
console.log('Is Production: ' + isProd);

if (isProd) {
  log.setLevel(log.levels.ERROR);
  console.log('Log level: ERROR');
} else {
  log.setLevel(log.levels.DEBUG);
  console.log('Log level: DEBUG');
}

const Logger = {
  console: log
};

export default Logger;
