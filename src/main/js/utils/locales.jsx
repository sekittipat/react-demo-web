import en_US from '../locales/en-us.jsx';
import th_TH from '../locales/th-th.jsx';

const locales = {
  en: en_US,
  th: th_TH
};

export default locales;
