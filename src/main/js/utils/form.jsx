import Logger from './logger.jsx';

export const formFieldsOnChanged = (event, formFieldsState) => {
  let fieldName = event.target.name;
  // Logger.console.debug('Field On Change', event, fieldName);
  let newState = Object.assign({}, formFieldsState);
  newState[fieldName].value = event.target.value;
  // Logger.console.debug('New State', newState);
  return newState;
};

export const formFieldsValidate = (formFieldStates, formFieldRefs) => {
  let results = {
    formFields: Object.assign({}, formFieldStates),
    error: false
  };

  let firstErrorField = false;
  Object.keys(results.formFields).map((fieldName, i) => {
    let field = results.formFields[fieldName];
    Logger.console.debug('Field Validate', field);
    if (field.required) {
      if (field.value == '') {
        field.error = true;
        // field.errorMsg = requiredMsg;
        results.formFields[fieldName] = field;
        results.error = true;

        if (!firstErrorField) {
          firstErrorField = fieldName;
        }
      } else {
        field.error = false;
        // field.errorMsg = '';
        results.formFields[fieldName] = field;
      }
    }
  });

  if (firstErrorField) {
    if (formFieldRefs && formFieldRefs[firstErrorField]) {
      formFieldRefs[firstErrorField].current.focus();
    }
  }

  return results;
};
