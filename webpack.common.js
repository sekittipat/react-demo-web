const path = require('path');

const AsyncChunkNames = require('webpack-async-chunk-names-plugin');
const CompressionPlugin = require('compression-webpack-plugin');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');

module.exports = {
  entry: './src/main/js/index.jsx',
  output: {
    path: path.resolve(__dirname, 'src/main/resources/static/built'),
    filename: 'bundle.js',
    chunkFilename: '[name].bundle.js'
  },
  resolve: {
    modules: ['node_modules'],
    alias: {
      JS: path.resolve(__dirname, 'src/main/js')
    }
  },
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        exclude: /(node_modules)/,
        use: [
          {
            loader: 'babel-loader',
            options: {
              presets: ['@babel/preset-env', '@babel/preset-react'],
              plugins: ['transform-class-properties', 'syntax-dynamic-import']
            }
          }
        ]
      },
      {
        test: /\.css$/,
        use: ['style-loader', 'css-loader']
      },
      {
        test: /\.scss$/,
        use: ['style-loader', 'css-loader', 'sass-loader']
      },
      {
        test: /\.(gif|ttf|eot|svg|woff2?)$/,
        use: 'url-loader?name=[name].[ext]'
      }
    ]
  },
  plugins: [
    new CleanWebpackPlugin(),
    new AsyncChunkNames(),
    new CompressionPlugin({
      filename: '[path].gz[query]',
      algorithm: 'gzip',
      test: /\.(js|css|html)$/,
      // compressionOptions: { level: 9 },
      threshold: 10240
      // minRatio: 0.8,
      // deleteOriginalAssets: false
    })
    // new BrotliPlugin({
    //   filename: '[path].br[query]',
    //   // algorithm: 'brotliCompress',
    //   test: /\.(js|css|html)$/,
    //   compressionOptions: { level: 11 },
    //   threshold: 10240,
    //   minRatio: 0.8,
    //   deleteOriginalAssets: false
    // })
  ],
  optimization: {
    splitChunks: {
      cacheGroups: {
        default: false,
        // vendor chunk
        vendor: {
          name: 'vendor',
          // sync + async chunks
          chunks: 'all',
          // import file path containing node_modules
          test: /[\\/]node_modules[\\/]/,
          // priority
          priority: 20
        },
        // common chunk
        common: {
          name: 'common',
          minChunks: 2,
          chunks: 'async',
          priority: 10,
          reuseExistingChunk: true,
          enforce: true
        }
      }
    }
  }
};
